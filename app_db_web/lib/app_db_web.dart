import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/app_db_idb.dart';
import 'package:idb_shim/idb_client_native.dart';

final DatabaseProvider databaseProviderWeb =
    getDatabaseProviderIdb(idbFactoryNative);
