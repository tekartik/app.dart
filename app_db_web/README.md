## Setup

```yaml
tekartik_app_db_web:
    git:
      url: https://gitlab.com/tekartik/app.dart
      path: app_db_web
    version: '>=0.1.0'
```