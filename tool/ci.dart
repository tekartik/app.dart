import 'package:process_run/shell.dart';

Future main() async {
  var shell = Shell();

  for (var dir in ['content', 'app_db', 'app_db_test', 'app_db_web']) {
    shell = shell.pushd(dir);
    await shell.run('''

pub get
dart tool/ci.dart

    ''');
    shell = shell.popd();
  }
}
