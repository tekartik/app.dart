import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/sembast/sembast_provider.dart';
import 'package:sembast/sembast_io.dart';
import 'package:test/test.dart';

import 'test_db_data.dart';

void main() {
  group('basic', () {
    test('persistent1', () async {
      var provider = getDatabaseProviderSembast(
          sembastDatabaseFactory: createDatabaseFactoryIo(
              rootPath: '.dart_tool/app_db/example/sembast'));
      var store = StoreRef('note');
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = DateTime.now().millisecondsSinceEpoch;
      var name = 'journal';
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbNote());
              }));
      var key = await store.addOld(db, note);

      print('added $key');
    });

    test('inMemory', () async {
      var provider = newDatabaseProviderMemory();
      var store = StoreRef('note');
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = DateTime.now().millisecondsSinceEpoch;
      var name = 'journal';
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbNote());
              }));
      var key = await store.add(db, note.toMap());
      var record = store.record(key);
      var map = {
        'title': 'my_title',
        'content': 'my_content',
        'date': note.date.v
      };
      expect(await record.get(db), map);

      var readNote = DbNote()..fromMap(map);
      expect(readNote, note);

      readNote = DbNote()..fromSnapshot(await record.getDbSnapshot(db));
      expect(Content.equals(readNote, note), isTrue);
      print('added $key');
      await db.close();

      // Re open
      db = await provider.openDatabase(name);
      expect(await record.get(db), map);
    });
  });
}
