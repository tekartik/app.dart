import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

void main() {
  group('Store', () {
    test('equals', () async {
      expect(StoreRef('name'), StoreRef('name'));
      expect(StoreRef('name'), isNot(StoreRef('name2')));
      try {
        StoreRef(null);
        fail('should fail');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
    });
  });
}
