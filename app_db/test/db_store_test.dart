import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

import 'test_db_data.dart';

void main() {
  group('DbStore', () {
    test('DbStore', () async {
      var db = await newInMemoryDatabase();
      var store = DbSchema.dbStore<DbNote>('note', builder: () => DbNote());
      var record = await store.add(db, DbNote()..title.v = 'my_title');
      var dbNote = await record.get(db);
      expect(dbNote.key, 1);
      expect(dbNote.title.v, 'my_title');
      dbNote.title.v = 'new_title';
      dbNote = await record.set(db, dbNote);
      expect(dbNote.title.v, 'new_title');
      expect(dbNote.key, 1);
      dbNote = await record.get(db);
      expect(dbNote.title.v, 'new_title');
    });
  });
}
