import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

void main() {
  group('Record', () {
    test('equals', () async {
      var store = StoreRef('store');
      var store2 = StoreRef('store2');
      expect(store.record(1), store.record(1));
      expect(store.record(1), StoreRef('store').record(1));
      expect(store.record(1), isNot(store.record(2)));
      expect(store.record(1), isNot(store2.record(1)));
      try {
        store.record(null);
        fail('should fail');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
    });
  });
}
