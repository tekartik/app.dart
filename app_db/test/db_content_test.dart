import 'package:test/test.dart';

import 'test_db_data.dart';

void main() {
  group('DbContent', () {
    test('toMap', () async {
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1;
      expect(note.toMap(),
          {'title': 'my_title', 'content': 'my_content', 'date': 1});
      expect(note.toMap(columns: [note.title.name]), {'title': 'my_title'});
    });
    test('equals', () {
      expect(DbNote(), DbNote());
      expect(DbNote(), isNot(DbNote()..key = 1));
      expect(DbNote()..key = 2, isNot(DbNote()..key = 1));
      expect(DbNote()..key = 1, DbNote()..key = 1);
      expect(DbNote(), isNot(DbNote()..title.v = 'my title'));
      expect(DbNote()..title.v = 'my title', DbNote()..title.v = 'my title');
    });
  });
}
