import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

void main() {
  group('Index', () {
    test('equals', () async {
      var store = StoreRef('store');
      var store2 = StoreRef('store2');
      var index1 = store.index(column: Column<String>('col1'));
      expect(index1, store.index(column: Column<String>('col1')));
      expect(index1, isNot(store2.index(column: Column<String>('col1'))));
      expect(index1, isNot(store.index(column: Column<String>('col2'))));
      try {
        store.index(column: null);
        fail('should fail');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
    });
  });
}
