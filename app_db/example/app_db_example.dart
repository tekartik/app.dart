import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/sembast/sembast_provider.dart';
import 'package:sembast/sembast_io.dart';

class DbNote extends DbContent {
  final title = stringField('title');
  final content = stringField('content');
  final date = intField('date');

  @override
  List<Field> get fields => [title, content, date];
}

Future main() async {
  var provider = getDatabaseProviderSembast(
      sembastDatabaseFactory: createDatabaseFactoryIo(
          rootPath: '.dart_tool/app_db/example/sembast'));
  var store = StoreRef('note');
  var note = DbNote()
    ..title.v = 'my_title'
    ..content.v = 'my_content'
    ..date.v = DateTime.now().millisecondsSinceEpoch;
  var name = 'journal';
  var db = await provider.openDatabase(name,
      options: DatabaseOptions(
          version: 1,
          onVersionChanged: (txn, oldVersion, newVersion) {
            txn.addStoreOld(store, DbNote());
          }));
  var key = await store.addOld(db, note);
  print('added $key');
}
