## Setup

```yaml
tekartik_app_db:
    git:
      url: https://gitlab.com/tekartik/app.dart
      path: app_db
    version: '>=0.1.0'
```