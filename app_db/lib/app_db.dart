/// Support for doing something awesome.
///
/// More dartdocs go here.
library tekartik_app_db;

import 'package:tekartik_app_db/src/provider.dart';
import 'package:tekartik_app_db/src/sembast/sembast_provider.dart' as sembast;

import 'src/db.dart';

export 'package:tekartik_app_content/content.dart';
export 'package:tekartik_app_db/src/boundary.dart' show Boundary;
export 'package:tekartik_app_db/src/db_content.dart'
    show DbContent, ContentWithIndexedFields;
export 'package:tekartik_common_utils/model/model.dart';

export 'src/db.dart'
    show
        Db,
        RecordExt,
        StoreExt,
        QueryExt,
        IndexQueryExt,
        TransactionMode,
        DbTransaction,
        DbClient,
        DbStoreExt,
        DbSchemaExt,
        DbRecordExt,
        DbIndexExt;
export 'src/db_column.dart' show DbColumnSchema;
export 'src/db_index.dart' show DbIndex, DbIndexSchema;
export 'src/db_model.dart' show DbModel;
export 'src/db_record.dart' show DbRecord;
export 'src/db_result_set.dart' show DbResultSet, DbPageCursor, PageCursor;
export 'src/db_schema.dart' show DbSchema, ColumnDbSchemaExtension;
export 'src/db_snapshot.dart' show DbSnapshot, DbSnapshotDev;
export 'src/db_store.dart' show DbStore, DbStoreSchema;
export 'src/filter.dart' show Filter;
export 'src/finder.dart' show Finder;
export 'src/index_query_ref.dart' show IndexQueryRef;
export 'src/index_ref.dart' show IndexRef;
export 'src/options.dart'
    show OpenTransaction, DatabaseOptions, DatabaseOpenMode;
export 'src/provider.dart' show DatabaseProvider;
export 'src/query_ref.dart' show QueryRef;
export 'src/query_ref.dart' show QueryRef;
export 'src/record_ref.dart' show RecordRef;
export 'src/record_snapshot.dart' show RecordSnapshot;
export 'src/result_set.dart' show ResultSet;
export 'src/store_ref.dart' show StoreRef;
export 'src/store_ref.dart' show StoreRef;
export 'src/tuple_column.dart' show TupleColumn, TupleColumn2;

/// An always empty database
Future<Db> newInMemoryDatabase() =>
    inMemoryDatabaseProvider.openDatabase(':memory:');

/// In memory provide, not sharing/persisting any data
DatabaseProvider get inMemoryDatabaseProvider =>
    sembast.inMemoryDatabaseProvider;
