import 'package:tekartik_app_db/app_db.dart';

/// Custom record reference holding to read/write snapshot
class DbRecord<T extends DbSnapshot> {
  final DbStore store;
  final int key;

  DbRecord(this.store, this.key);

  RecordRef get ref => store.ref.record(key);
}
