import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db_content.dart';

/// Dataase open mode
enum DatabaseOpenMode {
  /// Read-only
  readOnly,

  /// Create a new
  alwaysCreate,
}

abstract class OpenTransactionStore {
  /// Define the schema for the store
  OpenTransactionIndex<T> addIndex<T>(IndexRef<T> index);

  /// Define the schema for the store
  void addColumn<T>(Column<T> column);
}

abstract class OpenTransactionIndex<T> {}

abstract class OpenTransaction {
  /// Define the schema for the store
  OpenTransactionStore addStoreOld(StoreRef store, DbContent content);
  OpenTransactionStore addStore(StoreRef store);
  OpenTransactionStore store(StoreRef store);
}

/// To extend for custom implementation.
class DatabaseOptions {
  final DatabaseOpenMode mode;
  final int version;
  final DbSchema schema;
  final void Function(OpenTransaction db, int oldVersion, int newVersion)
      onVersionChanged;

  /// Version is set to 1 by default.
  DatabaseOptions(
      {this.mode, this.version = 1, this.onVersionChanged, this.schema});
}
