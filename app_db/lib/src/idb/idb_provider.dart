import 'package:idb_shim/idb_shim.dart' as idb;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/idb/idb_db.dart';
import 'package:tekartik_app_db/src/tuple_column.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

String idbIndexName(IndexRef index) =>
    '${columnsAsList(index.column).map((column) => column.name).join('_')}';

class DatabaseProviderIdb implements DatabaseProvider {
  final idb.IdbFactory idbFactory;

  DatabaseProviderIdb({@required this.idbFactory});

  @override
  Future<Db> openDatabase(String name, {DatabaseOptions options}) async {
    var db = await idbFactory.open(
      name,
      version: options?.version ?? 1,
      onUpgradeNeeded: (e) {
        var openTransaction = OpenTransactionIdb();
        options.onVersionChanged(openTransaction, e.oldVersion, e.newVersion);
        openTransaction.storesToAdd.forEach((store, txnStoreIdb) {
          var idbObjectStore =
              e.database.createObjectStore(store.name, autoIncrement: true);
          var indeciesTodAdd = txnStoreIdb.indeciesToAdd;
          indeciesTodAdd.forEach((index, txnIndexIdb) {
            idbObjectStore.createIndex(
                idbIndexName(index), columnsNamesOrName(index.column));
          });
        });
      },
    );
    return DbIdb(databaseProvider: this, idbDatabase: db);
  }

  @override
  Future deleteDatabase(String name) async {
    await idbFactory.deleteDatabase(name);
  }

  Future closeDatabase(Db db) async {
    var dbIdb = db as DbIdb;
    dbIdb.idbDatabase.close();
  }
}

DatabaseProvider getDatabaseProviderIdb(idb.IdbFactory idbFactory) =>
    DatabaseProviderIdb(idbFactory: idbFactory);
