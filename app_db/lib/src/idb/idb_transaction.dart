import 'package:idb_shim/idb.dart' as idb;
import 'package:meta/meta.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/db_result_set.dart';
import 'package:tekartik_app_db/src/idb/idb_client.dart';
import 'package:tekartik_app_db/src/index_ref.dart';
import 'package:tekartik_app_db/src/query_ref.dart';
import 'package:tekartik_app_db/src/record_ref.dart';
import 'package:tekartik_app_db/src/record_snapshot.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_app_db/src/store_ref.dart';

class TransactionIdb with DbClientIdbMixin implements DbTransaction {
  final idb.Transaction idbTransaction;

  TransactionIdb({@required this.idbTransaction});
  @override
  Future<int> add(StoreRef store, Map<String, dynamic> content) {
    return txnAdd(idbTransaction, store, content);
  }

  @override
  Future<RecordSnapshot> getSnapshot(RecordRef record) {
    return txnGet(idbTransaction, record);
  }

  @override
  Future<ResultSet> getQueryResultSet(QueryRef query, {PageCursor cursor}) {
    return txnGetQueryResultSet(idbTransaction, query, cursor: cursor);
  }

  @override
  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge}) {
    return txnSet(idbTransaction, record, content);
  }

  @override
  Future<RecordSnapshot> indexGetSnapshot<K>(IndexRef<K> index, K key) {
    return txnIndexGet(idbTransaction, index, key);
  }
}
