import 'package:idb_shim/idb.dart' as idb;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/idb/idb_db.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

mixin DbClientIdbMixin implements DbClient {
  Future<int> txnAdd(
      idb.Transaction txn, StoreRef store, Map<String, dynamic> content) async {
    var key = await txn.objectStore(store.name).add(content) as int;
    return key;
  }

  Future<RecordSnapshot> txnSet(idb.Transaction txn, RecordRef record,
      Map<String, dynamic> content) async {
    var key = await txn.objectStore(record.store.name).put(content, record.key)
        as int;
    return wrapSnapshot(key, content);
  }

  Future<RecordSnapshot> txnGet(idb.Transaction txn, RecordRef record) async {
    var value =
        (await txn.objectStore(record.store.name).getObject(record.key) as Map)
            ?.cast<String, dynamic>();

    if (value != null) {
      return wrapSnapshot(record.key, value);
    } else {
      return null;
    }
  }

  Future<RecordSnapshot> txnIndexGet<K>(
      idb.Transaction txn, IndexRef<K> index, K key) async {
    var txnStore = txn.objectStore(index.store.name);
    var txnIndex = txnStore.index(index.name);
    var recordKey = (await txnIndex.getKey(key)) as int;
    var value = (await txnIndex.get(key) as Map)?.cast<String, dynamic>();

    if (value != null) {
      return wrapSnapshot(recordKey, value);
    } else {
      return null;
    }
  }

  Future<ResultSet> txnGetQueryResultSet(idb.Transaction txn, QueryRef query,
      {PageCursor cursor}) async {
    var completer = Completer.sync();
    var snapshots = <RecordSnapshotIdb>[];
    void _complete() {
      if (!completer.isCompleted) {
        completer.complete();
      }
    }

    //var fieldName =

    var builder = QueryBuilderIdb();
    var limit = query?.finder?.limit;
    builder.offset = query?.finder?.offset;
    var count = 0;
    RecordSnapshotIdb lastSnapshot;
    var pageCursorIdb = unwrapPageCursor(cursor);

    if (pageCursorIdb != null) {
      builder.pageCursor = pageCursorIdb;
    }
    unawaited(txn
        .objectStore(query.store.name)
        .openCursor(range: builder.keyRange)
        .listen((cwv) {
          // Check filter first
          ++count;
          if (builder.offset != null) {
            if (count <= builder.offset) {
              cwv.next();
              return;
            }
          }

          if (limit != null) {
            if (snapshots.length >= limit) {
              _complete();
              return;
            }
          }

          lastSnapshot = wrapSnapshot(cwv.key as int, Model(cwv.value as Map));
          snapshots.add(lastSnapshot);

          if (limit != null) {
            if (snapshots.length >= limit) {
              _complete();
              return;
            }
          }
          cwv.next();
        })
        .asFuture()
        .then((_) {
          if (!completer.isCompleted) {
            // we reached the end
            _complete();
          }
        }));

    await completer.future;
    return ResultSetImpl(
        snapshots: snapshots,
        cursor: PageCursorIdb(lastSnapshot: lastSnapshot));
  }
}
