import 'package:idb_shim/idb_shim.dart' as idb;
import 'package:idb_sqflite/idb_sqflite.dart' as idb;
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/boundary.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/db_content.dart';
import 'package:tekartik_app_db/src/db_result_set.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/idb/idb_client.dart';
import 'package:tekartik_app_db/src/idb/idb_provider.dart';
import 'package:tekartik_app_db/src/idb/idb_transaction.dart';
import 'package:tekartik_app_db/src/options.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_app_db/src/tuple_column.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

class OpenTransactionIndexIdb<T> extends OpenTransactionIndex<T> {}

/// Global debug flag.
bool appDbIdbDebug = false;

class OpenTransactionStoreIdb extends OpenTransactionStore {
  final DbContent dbContent;
  final indeciesToAdd = <IndexRef, OpenTransactionIndexIdb<dynamic>>{};

  OpenTransactionStoreIdb([this.dbContent]);

  @override
  OpenTransactionIndex<T> addIndex<T>(IndexRef<T> index) {
    return indeciesToAdd[index] = OpenTransactionIndexIdb<T>();
  }

  @override
  void addColumn<T>(Column<T> column) {
    // Not needed on indexed db
  }
}

class OpenTransactionIdb extends OpenTransaction {
  final storesToAdd = <StoreRef, OpenTransactionStoreIdb>{};

  @override
  OpenTransactionStore addStoreOld(StoreRef store, DbContent content) {
    return storesToAdd[store] = OpenTransactionStoreIdb(content);
  }

  @override
  OpenTransactionStore addStore(StoreRef store) {
    return storesToAdd[store] = OpenTransactionStoreIdb();
  }

  @override
  OpenTransactionStore store(StoreRef store) {
    // TODO: implement store
    throw UnimplementedError();
  }
}

/*
Store _wrapStore(sembast.StoreRef<int, Map<String, dynamic>> storeRef) {
  return Store(name: storeRef.name);
}
*/

/*
sembast.StoreRef<int, Map<String, dynamic>> _unwrapStore(StoreRef store) =>
    sembast.intMapStoreFactory.store(store.name);

sembast.RecordRef<int, Map<String, dynamic>> _unwrapRecord(Record record) =>
    _unwrapStore(record.store).record(record.key);

class _RecordIdb {
  final int key;
  final Map<String, dynamic> value;

  _RecordIdb(this.key, this.value);
}
*/

class DbPageCursorIdb implements DbPageCursor {
  final DbSnapshot lastSnapshot;

  DbPageCursorIdb({@required this.lastSnapshot});
}

class PageCursorIdb implements PageCursor {
  final RecordSnapshotIdb lastSnapshot;

  PageCursorIdb({@required this.lastSnapshot});
}

/// Slow implementation, use custom type instead.
class RecordSnapshotIdb implements RecordSnapshot {
  @override
  final int key;
  @override
  final Map<String, dynamic> value;

  RecordSnapshotIdb(this.key, this.value);
}

/// Slow implementation, use custom type instead.
class DbSnapshotIdbOld
    with DbSnapshotMixin, ContentMixin, CvModelBaseMixin
    implements DbSnapshot {
  @override
  final int key;
  final Map<String, dynamic> value;

  List<Field> _fields;
  Map<String, Field> _fieldMap;

  DbSnapshotIdbOld(this.key, this.value);

  @override
  List<Field> get fields => _fields ??= () {
        value?.forEach((key, value) {
          _fieldMap ??= <String, Field>{};
          _fieldMap[key] = Field(key)..v = value;
        });
        return _fieldMap?.values?.toList(growable: false);
      }();

  @override
  Field<T> field<T>(String name) {
    // create map if needed
    fields;
    return _fieldMap[name]?.cast<T>();
  }
}

RecordSnapshotIdb wrapSnapshot(int key, Map<String, dynamic> value) {
  if (value == null) {
    return null;
  } else {
    return RecordSnapshotIdb(key, value);
  }
}

DbSnapshotIdbOld _wrapSnapshotOld(int key, Map<String, dynamic> value) {
  if (value == null) {
    return null;
  } else {
    return DbSnapshotIdbOld(key, value);
  }
}

class QueryBuilderIdb {
  int limit;
  int offset;
  idb.KeyRange keyRange;

  QueryBuilderIdb([Finder finder]) {
    if (finder != null) {
      limit = finder.limit;
      offset = finder.offset;
    }
  }

  set pageCursorOld(DbPageCursorIdb pageCursor) {
    if (pageCursor != null) {
      keyRange = idb.KeyRange.lowerBound(pageCursor.lastSnapshot.key, true);
    }
  }

  set pageCursor(PageCursorIdb pageCursor) {
    if (pageCursor != null) {
      keyRange = idb.KeyRange.lowerBound(pageCursor.lastSnapshot.key, true);
    }
  }
}

class IndexQueryBuilderIdb extends QueryBuilderIdb {
  final IndexQueryRef indexQuery;

  IndexQueryBuilderIdb(this.indexQuery) : super(indexQuery.finder);

  String get fieldName => indexQuery.column.name;

  bool _isTuple;

  bool get isTuple => _isTuple ?? (indexQuery.column is TupleColumn);

  // item or list
  dynamic get key => _key;

  @override
  set pageCursorOld(DbPageCursorIdb pageCursor) {
    if (pageCursor != null) {
      keyRange = idb.KeyRange.lowerBound(
          pageCursor.lastSnapshot.field(fieldName).v, false);
    }
  }

  dynamic _key;

  void setBoundaries(dynamic key, Boundary start, Boundary end,
      DbPageCursorIdb dbPageCursorIdb) {
    dynamic lowerBound;
    bool lowerBoundOpen;
    dynamic upperBound;
    bool upperBoundOpen;
    if (dbPageCursorIdb != null) {
      if (isTuple) {
        throw 'TODO';
      } else {
        lowerBound = dbPageCursorIdb.lastSnapshot.field(fieldName).v;
      }
      if (key != null) {
        upperBound = tupleValuesOrValue(key);
        upperBoundOpen = false;
      }
      offset = null;
    } else if (key != null) {
      _key = tupleValuesOrValue(key);
      return;
    } else if (start != null) {
      lowerBound = tupleValuesOrValue(start.value);
      lowerBoundOpen = !(start.include);
    }
    if (end != null) {
      upperBound = tupleValuesOrValue(end.value);
      upperBoundOpen = !(end.include);
    }

    if (lowerBound != null && upperBound != null) {
      keyRange = idb.KeyRange.bound(
          lowerBound, upperBound, lowerBoundOpen, upperBoundOpen);
    } else if (lowerBound != null) {
      keyRange = idb.KeyRange.lowerBound(lowerBound, lowerBoundOpen);
    } else if (upperBound != null) {
      keyRange = idb.KeyRange.upperBound(upperBound, upperBoundOpen);
    }
  }
}

DbPageCursorIdb _unwrapPageCursorOld(DbPageCursor cursor) =>
    cursor as DbPageCursorIdb;

PageCursorIdb unwrapPageCursor(PageCursor cursor) => cursor as PageCursorIdb;

class DbIdb with DbClientIdbMixin implements Db {
  final DatabaseProviderIdb databaseProvider;
  final idb.Database idbDatabase;

  DbIdb({@required this.databaseProvider, @required this.idbDatabase});

  @override
  Future close() async {
    await databaseProvider.closeDatabase(this);
  }

  @override
  Future<T> getSnapshotOld<T extends DbSnapshot>(RecordRef record,
      {T content}) async {
    var key = record.key;
    var txn = idbDatabase.transaction([record.store.name], idb.idbModeReadOnly);
    try {
      var value = (await txn
              .objectStore(record.store.name)
              .getObject(record.key) as Map)
          ?.cast<String, dynamic>();

      if (content == null) {
        return _wrapSnapshotOld(record.key, value) as T;
      } else {
        if (value == null) {
          return null;
        }
        assert(
            content is DbContent, 'content $content must be of type DbContent');
        var dbContent = content as DbContent;
        if (appDbIdbDebug) {
          print('OLD reading ${record.store.name} $key $value');
        }
        dbContent.fromMap(value);
        dbContent.key = record.key;
        return content;
      }
    } finally {
      await txn.completed;
    }
  }

  @override
  Future putContent(DbContent context) {
    // TODO: implement putSnapshot
    throw UnimplementedError();
  }

  @override
  Future<int> addContent(StoreRef store, DbContent content) async {
    var txn = idbDatabase.transaction([store.name], idb.idbModeReadWrite);
    var key = await txn.objectStore(store.name).add(content.toMap()) as int;
    return key;
  }

  @override
  Future<DbResultSet> getQuerySnapshotsOld(QueryRef query,
      {DbPageCursor cursor}) async {
    var txn = idbDatabase.transaction([query.store.name], idb.idbModeReadOnly);

    var completer = Completer.sync();
    var snapshots = <DbSnapshot>[];
    void _complete() {
      if (!completer.isCompleted) {
        completer.complete();
      }
    }

    //var fieldName =

    var builder = QueryBuilderIdb();
    var limit = query?.finder?.limit;
    builder.offset = query?.finder?.offset;
    var count = 0;
    var done = false;
    DbSnapshot lastSnapshot;
    var pageCursorIdb = _unwrapPageCursorOld(cursor);

    if (pageCursorIdb != null) {
      builder.pageCursorOld = pageCursorIdb;
    }
    unawaited(txn
        .objectStore(query.store.name)
        .openCursor(range: builder.keyRange)
        .listen((cwv) {
          // Check filter first
          ++count;
          if (builder.offset != null) {
            if (count <= builder.offset) {
              cwv.next();
              return;
            }
          }

          if (limit != null) {
            if (snapshots.length >= limit) {
              _complete();
              return;
            }
          }

          lastSnapshot =
              _wrapSnapshotOld(cwv.key as int, Model(cwv.value as Map));
          snapshots.add(lastSnapshot);

          if (limit != null) {
            if (snapshots.length >= limit) {
              _complete();
              return;
            }
          }
          cwv.next();
        })
        .asFuture()
        .then((_) {
          if (!completer.isCompleted) {
            // we reached the end
            done = true;
            _complete();
          }
        }));

    // TODO: implement getQuerySnapshots
    await completer.future;
    return DbResultSetImpl(
        snapshots: snapshots,
        done: done,
        cursor: DbPageCursorIdb(lastSnapshot: lastSnapshot));
  }

  @override
  Future<DbResultSet<DbSnapshot>> getIndexQuerySnapshots<T>(
      IndexQueryRef<T> index,
      {DbPageCursor cursor}) async {
    var txn = idbDatabase.transaction([index.store.name], idb.idbModeReadOnly);

    var completer = Completer.sync();
    var snapshots = <DbSnapshot>[];
    void _complete() {
      if (!completer.isCompleted) {
        completer.complete();
      }
    }

    var builder = IndexQueryBuilderIdb(index);
    builder.limit = index.finder?.limit;
    builder.offset = index.finder?.offset;

    var store = index.store;
    var indexName = idbIndexName(index.index);
    // TODO support tuple
    var fieldName = index.column.name;
    //var limit = finder?.limit;
    //var offset = finder?.offset;
    var count = 0;
    var done = false;
    DbSnapshot lastSnapshot;
    var pageCursorIdb = _unwrapPageCursorOld(cursor);

    builder.setBoundaries(index.key, index.start, index.end, pageCursorIdb);

    // Set to true when after cursor
    var pastCursor = false;
    unawaited(txn
        .objectStore(store.name)
        .index(indexName)
        .openCursor(key: builder.key, range: builder.keyRange)
        .listen((cwv) {
          // Ignore until last snapshot, when the key is not part of the path
          if (pageCursorIdb != null && !pastCursor) {
            var indexKeyValue = (cwv.value as Map)[fieldName] as Comparable;
            if (indexKeyValue
                    .compareTo(pageCursorIdb.lastSnapshot.field(fieldName).v) >
                0) {
              pastCursor = true;
            } else {
              var primaryKeyValue = cwv.primaryKey as int;
              if (primaryKeyValue > pageCursorIdb.lastSnapshot.key) {
                pastCursor = true;
              } else {
                cwv.next();
                return;
              }
            }
          }
          // Check filter first
          ++count;
          if (builder.offset != null) {
            if (count <= builder.offset) {
              cwv.next();
              return;
            }
          }

          if (builder.limit != null) {
            if (snapshots.length >= builder.limit) {
              _complete();
              return;
            }
          }

          lastSnapshot =
              _wrapSnapshotOld(cwv.primaryKey as int, Model(cwv.value as Map));
          snapshots.add(lastSnapshot);

          if (builder.limit != null) {
            if (snapshots.length >= builder.limit) {
              _complete();
              return;
            }
          }
          cwv.next();
        })
        .asFuture()
        .then((_) {
          if (!completer.isCompleted) {
            // we reached the end
            done = true;
            _complete();
          }
        }));

    // TODO: implement getQuerySnapshots
    await completer.future;
    return DbResultSetImpl(
        snapshots: snapshots,
        done: done,
        cursor: DbPageCursorIdb(lastSnapshot: lastSnapshot));
  }

  @override
  Future clear(StoreRef store) async {
    var txn = idbDatabase.transaction(store.name, idb.idbModeReadWrite);
    try {
      var objectStore = txn.objectStore(store.name);
      await objectStore.clear();
    } finally {
      await txn.completed;
    }
  }

  @override
  Future<int> add(StoreRef store, Map<String, dynamic> content) async {
    var txn = idbDatabase.transaction([store.name], idb.idbModeReadWrite);
    try {
      return txnAdd(txn, store, content);
    } finally {
      await txn.completed;
    }
  }

  @override
  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge}) async {
    var txn =
        idbDatabase.transaction([record.store.name], idb.idbModeReadWrite);
    try {
      // TODO handle merge
      await txnSet(txn, record, content);
      return wrapSnapshot(record.key, content);
    } finally {
      await txn.completed;
    }
  }

  @override
  Future<RecordSnapshot> getSnapshot(RecordRef record) async {
    var txn = idbDatabase.transaction([record.store.name], idb.idbModeReadOnly);
    try {
      return txnGet(txn, record);
    } finally {
      await txn.completed;
    }
  }

  @override
  Future<T> transaction<T>(FutureOr<T> Function(DbTransaction txn) action,
      {List<StoreRef> stores, TransactionMode mode}) async {
    var storeNames = List<String>.from(
        stores?.map((e) => e.name) ?? idbDatabase.objectStoreNames);
    var idbTransaction = idbDatabase.transaction(
        storeNames,
        (mode == TransactionMode.readWrite)
            ? idb.idbModeReadWrite
            : idb.idbModeReadOnly);
    var transactionIdb = TransactionIdb(idbTransaction: idbTransaction);
    T result;
    try {
      result = await action(transactionIdb);
    } finally {
      await idbTransaction.completed;
    }

    return result;
  }

  @override
  Future<ResultSet> getQueryResultSet(QueryRef query,
      {PageCursor cursor}) async {
    var txn = idbDatabase.transaction([query.store.name], idb.idbModeReadOnly);
    return txnGetQueryResultSet(txn, query, cursor: cursor);
  }

  @override
  Future<RecordSnapshot> indexGetSnapshot<K>(IndexRef<K> index, K key) async {
    var txn = idbDatabase.transaction([index.store.name], idb.idbModeReadOnly);
    try {
      return txnIndexGet(txn, index, key);
    } finally {
      await txn.completed;
    }
  }
}
