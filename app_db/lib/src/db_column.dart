import 'package:tekartik_app_db/app_db.dart';

class DbColumnSchema {
  final Column column;

  DbColumnSchema({this.column});

  String get name => column.name;

  @override
  String toString() => '<${column.type}> ${column.name}';
}
