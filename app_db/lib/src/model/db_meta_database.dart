import 'package:tekartik_app_db/app_db.dart';

abstract class DbMetaContentBase extends DbContent {
  final name = stringField('name');
  @override
  List<Field> get fields => [name];
}

class DbMetaModel extends DbMetaContentBase {}

class DbMetaDatabaseContent extends DbMetaContentBase {
  /// Internal version in store meta
  final version = intField('app_db');
  @override
  List<Field> get fields => [...super.fields, version];
}
