import 'package:meta/meta.dart';
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_db/app_db.dart';

/// Record snapshot
abstract class DbSnapshot implements Content {
  /// to override something like [name, description]
  @override
  List<Field> get fields;

  /// The record id
  int get key;

  /// Access content by name field
  @override
  Field<T> field<T>(String name);

  /// store constructor.

  /// Store with builder
  @Deprecated('use DbSchema.dbStore instead')
  static DbStore<T> store<T extends DbSnapshot>(String name,
      {@required T Function() builder}) {
    return DbStore<T>(StoreRef(name), builder);
  }

  static bool contentEquals(DbSnapshot snapshot1, DbSnapshot snapshot2) {
    return false;
  }
}

/// Slow implementation, use custom type instead.
mixin DbSnapshotMixin implements DbSnapshot {
  /*
  Map<String, Field> _fieldMap;

  @override
  Field<T> field<T>(String name) {
    // create map if needed
    fields;
    return _fieldMap[name]?.cast<T>();
  }
  @override
  Model toMap({List<String> columns}) {
    columns ??= fields.names;
    var model = Model();
    for (var column in columns) {
      var field = this.field(column);
      model.setValue(field.name, field.v, presentIfNull: field.hasValue);
    }
    return model;
  }

  @override
  String toString() => '$key ${toMap()}';

   */
}

/// Slow implementation, use custom type instead.
class DbSnapshotImpl
    with DbSnapshotMixin, ContentMixin, CvModelBaseMixin
    implements DbSnapshot {
  @override
  final int key;
  final Map<String, dynamic> value;

  List<Field> _fields;
  Map<String, Field> _fieldMap;

  DbSnapshotImpl(this.key, this.value);

  @override
  List<Field> get fields => _fields ??= () {
        value?.forEach((key, value) {
          _fieldMap ??= <String, Field>{};
          _fieldMap[key] = Field(key)..v = value;
        });
        return _fieldMap?.values?.toList(growable: false);
      }();

  @override
  Field<T> field<T>(String name) {
    // create map if needed
    fields;
    return _fieldMap[name]?.cast<T>();
  }
}

extension DbSnapshotDev on DbSnapshot {
  Model toDebugMap({List<String> columns}) {
    var map =
        Model(Map.from((this as DbSnapshotMixin).toMap(columns: columns)));
    map['_id'] = key;
    // ???
    map.remove('_v');
    return map;
  }
}
