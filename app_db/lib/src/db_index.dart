import 'package:tekartik_app_db/app_db.dart';

import 'index_ref.dart';

class DbIndex<T extends DbSnapshot, K> {
  final DbStore<T> store;
  final Column<K> column;
  IndexRef<K> get ref => IndexRefImpl<K>(store: store.ref, column: column);

  DbIndex(this.store, this.column);

  DbIndexSchema get schema => DbIndexSchema(column: column.schema);

  @override
  String toString() => ref.toString();
}

class DbIndexSchema {
  DbColumnSchema column;

  DbIndexSchema({this.column});
}
