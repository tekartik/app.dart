import 'package:meta/meta.dart';
import 'package:tekartik_app_db/src/store_ref.dart';

///
/// An immutable record reference
///
abstract class RecordRef {
  /// Store reference.
  StoreRef get store;

  /// Record key, null for new record.
  int get key;
}

class RecordRefImpl implements RecordRef {
  @override
  final int key;

  RecordRefImpl({@required this.key, @required this.store}) {
    assert(key != null, 'store $store record key cannot be null');
  }

  @override
  final StoreRef store;

  @override
  int get hashCode => key;
  @override
  bool operator ==(other) {
    if (other is RecordRefImpl) {
      if (other.key != key) {
        return false;
      }
      if (other.store != store) {
        return false;
      }
      return true;
    }
    return false;
  }
}

///
abstract class Records {
  /// Store reference.
  StoreRef get store;

  /// Record key, null for new record.
  List get keys;
}
