import 'package:meta/meta.dart';
import 'package:tekartik_app_db/app_db.dart';

import 'db_store.dart';

class DbSchema {
  final List<DbStoreSchema> stores;

  /// Only present with DbContent helpers
  final List<DbStore> dbStores;
  final _map = <String, DbStoreSchema>{};
  DbSchema({this.stores}) : dbStores = null {
    stores.forEach((store) {
      _map[store.name] = store;
    });
  }

  DbStoreSchema store(String name) => _map[name];

  /// Store with builder
  static DbStore<T> dbStore<T extends DbSnapshot>(String name,
      {@required T Function() builder}) {
    return DbStore<T>(StoreRef(name), builder);
  }

  /// Db helper
  DbSchema.db({List<DbStore> stores})
      : stores = stores.map((e) => e.schema).toList(),
        dbStores = stores;
}

extension ColumnDbSchemaExtension on Column {
  DbColumnSchema get schema {
    return DbColumnSchema(column: this);
  }
}
