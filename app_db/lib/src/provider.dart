import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db.dart';

abstract class DatabaseProvider {
  /// No options means readOnly
  Future<Db> openDatabase(String name, {DatabaseOptions options});

  /// Never fails
  Future deleteDatabase(String name);
}
