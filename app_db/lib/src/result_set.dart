import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/record_snapshot.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

abstract class ResultSet implements List<RecordSnapshot> {
  PageCursor get cursor;
}

class ResultSetImpl with ListMixin<RecordSnapshot> implements ResultSet {
  final List<RecordSnapshot> _snapshots;
  @override
  int get length => _snapshots.length;

  ResultSetImpl({List<RecordSnapshot> snapshots, this.cursor})
      : _snapshots = snapshots;

  @override
  RecordSnapshot operator [](int index) => _snapshots[index];

  @override
  void operator []=(int index, RecordSnapshot value) {
    throw UnsupportedError('read-only');
  }

  @override
  final PageCursor cursor;

  @override
  set length(int newLength) {
    throw UnsupportedError('read-only');
  }
}

extension ResultSetDev on ResultSet {
  List<Model> toDebugMap() => map((snapshot) => snapshot.toDebugMap()).toList();
}
