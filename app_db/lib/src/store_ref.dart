import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/index_ref.dart';
import 'package:tekartik_app_db/src/query_ref.dart';
import 'package:tekartik_app_db/src/record_ref.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

/// A pointer to a store.
///
/// Provides access helper to data on the store using a given [DatabaseClient].
///
abstract class StoreRef {
  /// The name of the store
  String get name;

  /// Create a record reference.
  ///
  /// Key cannot be null.
  RecordRef record(int key);

  /// Create a reference to multiple records
  ///
  Records records(Iterable<int> keys);

  factory StoreRef(String name) => StoreRefImpl(name: name);

  /// Create query ref
  QueryRef query({Finder finder});

  /// Create an index ref
  IndexRef<T> index<T>({@required Column<T> column});
}

class StoreRefImpl implements StoreRef {
  @override
  final String name;

  StoreRefImpl({@required this.name}) {
    assert(name != null, 'store name cannot be null');
  }

  @override
  RecordRef record(int key) => RecordRefImpl(store: this, key: key);

  @override
  Records records(Iterable<int> keys) {
    // TODO: implement records
    throw UnimplementedError();
  }

  @override
  int get hashCode => name?.hashCode;
  @override
  bool operator ==(other) {
    if (other is StoreRefImpl) {
      return other.name == name;
    }
    return false;
  }

  @override
  QueryRef query({Finder finder}) => QueryRefImpl(finder: finder, store: this);

  @override
  IndexRef<T> index<T>({Column<T> column}) =>
      IndexRefImpl(store: this, column: column);

  @override
  String toString() => 'Store($name)';
}
