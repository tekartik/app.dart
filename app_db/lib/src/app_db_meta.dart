import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/model/db_meta_database.dart';

var metaStore = DbSchema.dbStore('__meta', builder: () => DbMetaModel());
var model = metaStore.model;
var index = metaStore.index<String>(column: model.name);
