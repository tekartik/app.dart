import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

/// Page query cursor.
class DbPageCursor {}

/// Page query cursor.
class PageCursor {}

/// Result list
abstract class DbResultSet<T extends DbSnapshot> implements List<T> {
  /// When done is set, cursor is no null.
  bool get done;

  /// Cursor to use for next query
  DbPageCursor get cursor;
}

class DbResultSetImpl<T extends DbSnapshot>
    with ListMixin<T>
    implements DbResultSet<T> {
  final List<T> snapshots;
  @override
  final DbPageCursor cursor;

  @override
  final bool done;

  DbResultSetImpl(
      {@required this.snapshots, @required this.cursor, @required this.done});

  @override
  int get length => snapshots.length;

  @override
  T operator [](int index) => snapshots[index];

  @override
  void operator []=(int index, T value) {
    throw UnsupportedError('read-only');
  }

  @override
  set length(int newLength) {
    throw UnsupportedError('read-only');
  }

  Model toDebugMap() => Model()
    ..setValue('done', done)
    ..setValue('cursor', cursor.toString())
    ..setValue('snapshots', snapshots.length);
  @override
  String toString() => toDebugMap().toString();
}
