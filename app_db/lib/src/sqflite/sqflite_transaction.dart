import 'package:meta/meta.dart';
import 'package:sqflite_common/sqlite_api.dart' as sqflite;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_client.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_db.dart';

class TransactionSqflite with DbClientSqfliteMixin implements DbTransaction {
  final DbSqflite db;
  TransactionSqflite({@required this.db, @required this.sqfliteClient});

  @override
  final sqflite.DatabaseExecutor sqfliteClient;

  @override
  DatabaseOptions get options => db.options;
}
