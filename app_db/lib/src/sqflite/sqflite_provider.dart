import 'package:sqflite_common/sqlite_api.dart' as sqflite;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_db.dart';
import 'package:tekartik_app_db/src/tuple_column.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

// ignore: unused_element
String _fieldSqlType(Column column) {
  if (column.isTypeInt) {
    return 'INTEGER';
  } else if (column.isTypeString) {
    return 'TEXT';
  }
  throw UnsupportedError('unsupported column $column');
}

String _indexName(IndexRef index) =>
    '${index.store.name}_${columnsNames(index.column).join('_')}';

class DatabaseProviderSqflite implements DatabaseProvider {
  final sqflite.DatabaseFactory sqfliteFactory;

  DatabaseProviderSqflite({@required this.sqfliteFactory});

  @override
  Future<Db> openDatabase(String name, {DatabaseOptions options}) async {
    var db = await sqfliteFactory.openDatabase(name,
        options: sqflite.OpenDatabaseOptions(
            version: options?.version ?? 1,
            onCreate: (db, version) async {
              var openTransaction = OpenTransactionSqflite();
              options.onVersionChanged(openTransaction, 0, version);
              var batch = db.batch();
              openTransaction.storesToAdd.forEach((store, value) {
                var sqlTable = store.name;
                batch.execute('DROP TABLE IF EXISTS ${store.name}');
                batch.execute(
                    'CREATE TABLE $sqlTable (_id INTEGER PRIMARY KEY, _v TEXT)');

                void _addColumn(Column column) {
                  var sqlType = _fieldSqlType(column);
                  var sqlName = column.name;
                  batch.execute(
                      'ALTER TABLE $sqlTable ADD COLUMN $sqlName $sqlType');
                }

                if (value.columnsToAdd.isNotEmpty ?? false) {
                  for (var column in value.columnsToAdd) {
                    _addColumn(column);
                  }
                }
                if (value.indeciesToAdd.isNotEmpty ?? false) {
                  for (var index in value.indeciesToAdd.keys) {
                    var columns = columnsAsList(index.column);
                    for (var i = 0; i < columns.length; i++) {
                      var column = columns[i];
                      _addColumn(column);
                    }
                    var indexName = _indexName(index);
                    batch.execute('DROP INDEX IF EXISTS $indexName');
                    batch.execute(
                        'CREATE INDEX $indexName ON ${store.name} (${columnsNames(index.column).join(', ')})');
                  }
                }
              });
              await batch.commit(noResult: true);
              // devPrint(await db.query('sqlite_master'));
            }));

    return DbSqflite(
        databaseProvider: this, sqfliteDatabase: db, options: options);
  }

  @override
  Future deleteDatabase(String name) async {
    await sqfliteFactory.deleteDatabase(name);
  }

  Future closeDatabase(Db db) async {
    var dbSqflite = db as DbSqflite;
    await dbSqflite.sqfliteDatabase.close();
  }
}

DatabaseProvider getDatabaseProviderSqflite(
        sqflite.DatabaseFactory sqfliteFactory) =>
    DatabaseProviderSqflite(sqfliteFactory: sqfliteFactory);
