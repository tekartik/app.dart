import 'package:sqflite_common/sqlite_api.dart' as sqflite;
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/boundary.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/db_result_set.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/options.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_client.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_provider.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_transaction.dart';
import 'package:tekartik_app_db/src/tuple_column.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

/// Global debug flag.
bool appDbSqfliteDebug = false;

class OpenTransactionIndexSqflite<T> extends OpenTransactionIndex<T> {}

class OpenTransactionStoreSqflite extends OpenTransactionStore {
  final DbContent dbContent;
  final indeciesToAdd = <IndexRef, OpenTransactionIndexSqflite<dynamic>>{};
  final columnsToAdd = <Column>{};

  OpenTransactionStoreSqflite([this.dbContent]);

  @override
  OpenTransactionIndex<T> addIndex<T>(IndexRef<T> index) {
    return indeciesToAdd[index] = OpenTransactionIndexSqflite<T>();
  }

  @override
  void addColumn<T>(Column<T> column) {
    columnsToAdd.add(column);
  }
}

class OpenTransactionSqflite extends OpenTransaction {
  final storesToAdd = <StoreRef, OpenTransactionStoreSqflite>{};

  @override
  OpenTransactionStore addStoreOld(StoreRef store, DbContent content) {
    return storesToAdd[store] = OpenTransactionStoreSqflite(content);
  }

  @override
  OpenTransactionStore addStore(StoreRef store) {
    return storesToAdd[store] = OpenTransactionStoreSqflite();
  }

  @override
  OpenTransactionStore store(StoreRef store) {
    // TODO: implement store
    throw UnimplementedError();
  }
}

/*
class DbPageCursorSqflite extends DbPageCursor {

}

 */

class RecordSnapshotSqflite implements RecordSnapshot {
  @override
  final int key;

  @override
  final Map<String, dynamic> value;

  RecordSnapshotSqflite(this.key, this.value);
}

/// Slow implementation, use custom type instead.
class DbSnapshotSqfliteOld
    with DbSnapshotMixin, ContentMixin, CvModelBaseMixin
    implements DbSnapshot {
  @override
  final int key;
  final Map<String, dynamic> value;

  List<Field> _fields;
  Map<String, Field> _fieldMap;

  DbSnapshotSqfliteOld(this.key, this.value);

  @override
  List<Field> get fields => _fields ??= () {
        value?.forEach((key, value) {
          _fieldMap ??= <String, Field>{};
          _fieldMap[key] = Field(key)..v = value;
        });
        return _fieldMap?.values?.toList(growable: false);
      }();

  @override
  Field<T> field<T>(String name) {
    // create map if needed
    fields;
    return _fieldMap[name]?.cast<T>();
  }
}

class DbPageCursorSqflite implements DbPageCursor {
  final DbSnapshotSqfliteOld snapshotSqflite;

  DbPageCursorSqflite({@required this.snapshotSqflite});
}

class PageCursorSqflite implements PageCursor {
  final RecordSnapshotSqflite snapshotSqflite;

  PageCursorSqflite({@required this.snapshotSqflite});
}

DbSnapshotSqfliteOld _wrapSnapshotOldFromRow(Map<String, dynamic> row) {
  var snapshot = wrapSnapshotFromRow(row);
  if (snapshot == null) {
    return null;
  } else {
    return DbSnapshotSqfliteOld(snapshot.key, snapshot.value);
  }
}

RecordSnapshotSqflite wrapSnapshot(int key, Map<String, dynamic> value) {
  if (value == null) {
    return null;
  } else {
    return RecordSnapshotSqflite(key, value);
  }
}

RecordSnapshotSqflite wrapSnapshotFromRow(Map<String, dynamic> row) {
  var key = row['_id'] as int;
  var value = (jsonDecode(row['_v'] as String) as Map)?.cast<String, dynamic>();
  if (value == null) {
    return null;
  } else {
    return wrapSnapshot(key, value);
  }
}

class IndexQueryBuilderSqflite extends QueryBuilderSqflite {
  @override
  String get keyPath => indexQuery.column.name;
  final IndexQueryRef indexQuery;

  IndexQueryBuilderSqflite(this.indexQuery, [Finder finder]) : super(finder);

  set key(dynamic key) {
    if (key != null) {
      var columns = columnsAsList(indexQuery.column);
      var keys = tupleValues(key);
      var conditions = <String>[];
      for (var i = 0; i < columns.length; i++) {
        conditions.add('${columns[i].name} = ?');
        whereArgs.add(keys[i]);
      }
      var where = conditions.join(' AND ');
      if (conditions.length > 1) {
        where = '($where)';
      }
      appendWhere(where);
    }
  }

  @override
  set pageCursorOld(DbPageCursorSqflite pageCursor) {
    if (pageCursor != null) {
      //keyRange = KeyRange.lowerBound(pageCursor.lastSnapshot.key, true);
      var keyValue = pageCursor.snapshotSqflite.value[keyPath];
      var primaryKeyValue = pageCursor.snapshotSqflite.key;
      appendWhere(
          '($keyPath > ? OR ($keyPath = ? AND _id > $primaryKeyValue))');
      whereArgs.add(keyValue);
      whereArgs.add(keyValue);
      // Remove offset
      offset = null;
    }
  }

  @override
  set start(Boundary start) {
    if (start != null) {
      if (indexQuery.column is TupleColumn) {
        // lower
        var sbLower = StringBuffer();
        var keyColumns = columnsAsList(indexQuery.column);
        var keyValues = tupleValues(start.value);

        for (var i = 0; i < keyColumns.length; i++) {
          var column = keyColumns[i].name;
          var key = keyValues[i];

          // last
          if (i == keyColumns.length - 1) {
            if (!(start.include)) {
              sbLower.write('$column > ?');
            } else {
              sbLower.write('$column >= ?');
            }
            whereArgs.add(key);
          } else {
            sbLower.write('($column > ?) OR ($column = ? AND (');
            whereArgs.addAll([key, key]);
          }
        }

        // close parenthesis
        for (var i = 1; i < keyColumns.length; i++) {
          sbLower.write('))');
        }

        appendWhere(sbLower.toString());
      } else {
        appendWhere('$keyPath ${start.include ? '>=' : '>'} ?');
        whereArgs.add(start.value);
      }
    }
  }

  set end(Boundary end) {
    if (end != null) {
      if (indexQuery.column is TupleColumn) {
        // upper
        var keyColumns = columnsAsList(indexQuery.column);
        var keyValues = tupleValues(end.value);

        var sbUpper = StringBuffer();

        for (var i = 0; i < keyColumns.length; i++) {
          var column = keyColumns[i].name;
          var key = keyValues[i];

          // last
          if (i == keyColumns.length - 1) {
            if (!(end.include)) {
              sbUpper.write('$column < ?');
            } else {
              sbUpper.write('$column <= ?');
            }
            whereArgs.add(key);
          } else {
            sbUpper.write('($column < ?) OR ($column = ? AND (');
            whereArgs.addAll([key, key]);
          }
        }

        // close parenthesis
        for (var i = 1; i < keyColumns.length; i++) {
          sbUpper.write('))');
        }

        appendWhere(sbUpper.toString());
      } else {
        appendWhere('$keyPath ${end.include ? '<=' : '<'} ?');
        whereArgs.add(end.value);
      }
    }
  }
}

class QueryBuilderSqflite {
  int limit;
  int offset;
  String where;
  final whereArgs = [];

  String get keyPath => '_id';

  void appendWhere(String where) {
    if (this.where == null) {
      this.where = where;
    } else {
      var sb = StringBuffer();
      sb.write('(');
      sb.write(this.where);
      sb.write(') AND (');
      sb.write(where);
      sb.write(')');
      this.where = sb.toString();
    }
  }

  QueryBuilderSqflite([Finder finder]) {
    if (finder != null) {
      limit = finder.limit;
      offset = finder.offset;
    }
  }

  set pageCursorOld(DbPageCursorSqflite pageCursor) {
    if (pageCursor != null) {
      //keyRange = KeyRange.lowerBound(pageCursor.lastSnapshot.key, true);
      where = '$keyPath > ${pageCursor.snapshotSqflite.key}';
      // Remove offset
      offset = null;
    }
  }

  set pageCursor(PageCursorSqflite pageCursor) {
    if (pageCursor != null) {
      //keyRange = KeyRange.lowerBound(pageCursor.lastSnapshot.key, true);
      where = '$keyPath > ${pageCursor.snapshotSqflite.key}';
      // Remove offset
      offset = null;
    }
  }

  set start(Boundary start) {
    if (start != null) {
      appendWhere('$keyPath ${start.include ? '>=' : '>'} ?');
      whereArgs.add(start.value);
    }
  }

  @override
  String toString() {
    var sb = StringBuffer('SELECT');
    if (where != null) {
      sb.write(' WHERE $where');
    }
    if (limit != null) {
      sb.write(' LIMIT $limit');
    }
    if (offset != null) {
      sb.write(' OFFSET $offset');
    }
    return sb.toString();
  }
}

class DbSqflite with DbClientSqfliteMixin implements Db {
  final DatabaseProviderSqflite databaseProvider;
  final sqflite.Database sqfliteDatabase;

  DbSqflite(
      {@required this.databaseProvider,
      @required this.sqfliteDatabase,
      @required this.options});

  @override
  Future close() async {
    await databaseProvider.closeDatabase(this);
  }

  @override
  Future<T> getSnapshotOld<T extends DbSnapshot>(RecordRef record,
      {T content}) async {
    var results = await sqfliteDatabase.query(record.store.name,
        where: '_id = ${record.key}');
    if (results.isNotEmpty) {
      var row = results.first;
      if (content == null) {
        return _wrapSnapshotOldFromRow(row) as T;
      } else {
        var dbContent = content as DbContent;
        dbContent.fromMap(asModel(jsonDecode(row['_v'] as String)));
        dbContent.key = record.key;
        return content;
      }
    }
    return null;
  }

  @override
  Future putContent(DbContent context) {
    // TODO: implement putSnapshot
    throw UnimplementedError();
  }

  @override
  Future<int> addContent(StoreRef store, DbContent content) async {
    assert(content.key == null, 'content must be new');
    var id = await add(store, content.toMap());
    return id;
  }

  @override
  Future<DbResultSet> getQuerySnapshotsOld(QueryRef query,
      {DbPageCursor cursor}) async {
    var builder = QueryBuilderSqflite(query.finder);
    if (cursor != null) {
      builder.pageCursorOld = cursor as DbPageCursorSqflite;
    } else {
      // TODO
    }
    var results = await sqfliteDatabase.query(query.store.name,
        where: builder.where,
        whereArgs: builder.whereArgs,
        offset: builder.offset,
        limit: builder.limit);
    var list = <DbSnapshot>[];
    DbSnapshotSqfliteOld lastSnapshot;
    for (var row in results) {
      lastSnapshot = _wrapSnapshotOldFromRow(row);
      list.add(lastSnapshot);
    }
    var done = list.length < (builder.limit ?? 1);
    var pageCursor = list.isNotEmpty
        ? DbPageCursorSqflite(snapshotSqflite: lastSnapshot)
        : null;
    return DbResultSetImpl(snapshots: list, done: done, cursor: pageCursor);
  }

  @override
  Future<DbResultSet<DbSnapshot>> getIndexQuerySnapshots<T>(
      IndexQueryRef<T> index,
      {DbPageCursor cursor}) async {
    var finder = index.finder;
    var builder = IndexQueryBuilderSqflite(index, finder);
    builder.key = index.key;
    if (cursor != null) {
      builder.pageCursorOld = cursor as DbPageCursorSqflite;
    } else {
      builder.start = index.start;
    }
    builder.end = index.end;

    var results = await sqfliteDatabase.query(index.store.name,
        where: builder.where,
        whereArgs: builder.whereArgs,
        offset: builder.offset,
        limit: builder.limit);
    var list = <DbSnapshot>[];
    DbSnapshotSqfliteOld lastSnapshot;
    for (var row in results) {
      lastSnapshot = _wrapSnapshotOldFromRow(row);
      list.add(lastSnapshot);
    }
    var done = list.length < (builder.limit ?? 1);
    var pageCursor = list.isNotEmpty
        ? DbPageCursorSqflite(snapshotSqflite: lastSnapshot)
        : null;
    return DbResultSetImpl(snapshots: list, done: done, cursor: pageCursor);
  }

  @override
  Future clear(StoreRef store) async {
    await sqfliteDatabase.delete(store.name);
  }

  @override
  Future<T> transaction<T>(FutureOr<T> Function(DbTransaction txn) action,
      {List<StoreRef> stores, TransactionMode mode}) {
    return sqfliteDatabase.transaction((sqfliteTransaction) async {
      var txn = TransactionSqflite(sqfliteClient: sqfliteTransaction, db: this);
      return await action(txn);
    });
  }

  @override
  sqflite.DatabaseExecutor get sqfliteClient => sqfliteDatabase;

  @override
  final DatabaseOptions options;
}
