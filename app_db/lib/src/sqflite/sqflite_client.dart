import 'dart:async';
import 'dart:convert';

import 'package:sqflite_common/sqlite_api.dart' as sqflite;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_db.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

mixin DbClientSqfliteMixin implements DbClient {
  DatabaseOptions get options;
  sqflite.DatabaseExecutor get sqfliteClient;
  @override
  Future<int> add(StoreRef store, Map<String, dynamic> content) async {
    var storeSchema = options?.schema?.store(store.name);
    var insertContent =
        _toSqliteInsertContent(null, content, storeSchema: storeSchema);
    var id = await sqfliteClient.insert(store.name, insertContent);
    if (appDbSqfliteDebug) {
      print('inserted: ${store.name} $id $insertContent');
    }

    return id;
  }

  Map<String, dynamic> _toSqliteUpdateContent(Map<String, dynamic> content,
      {DbStoreSchema storeSchema}) {
    var map = <String, dynamic>{'_v': jsonEncode(content)};
    // Add index and columns
    if (storeSchema != null) {
      if (storeSchema.indecies != null) {
        for (var index in storeSchema.indecies) {
          var name = index.column.name;
          map[name] = content[name];
        }
      }
      if (storeSchema.indecies != null) {
        for (var index in storeSchema.indecies) {
          var name = index.column.name;
          map[name] = content[name];
        }
      }
    }
    return map;
  }

  Map<String, dynamic> _toSqliteInsertContent(
      int key, Map<String, dynamic> content,
      {DbStoreSchema storeSchema}) {
    var map = _toSqliteUpdateContent(content, storeSchema: storeSchema);
    if (key != null) {
      map['_id'] = key;
    }
    return map;
  }

  Future<Map<String, dynamic>> _put(
      StoreRef store, int key, Map<String, dynamic> content) async {
    var updateContent = _toSqliteUpdateContent(content);
    var count = await sqfliteClient.update(store.name, updateContent,
        where: '_id = $key');
    // Not found, run in transaction
    if (count == 0) {
      await inTransaction((txn) async {
        var count = await txn.sqfliteClient.update(store.name, updateContent);
        // Not found, run in transaction
        if (count == 0) {
          // Try again or insert
          await txn.sqfliteClient
              .insert(store.name, _toSqliteInsertContent(key, content));
          return content;
        }
      });
    }
    return content;
  }

  @override
  Future<RecordSnapshot> getSnapshot(RecordRef record) async {
    var results = await sqfliteClient.query(record.store.name,
        where: '_id = ${record.key}');
    if (results.isNotEmpty) {
      var row = results.first;
      return wrapSnapshotFromRow(row);
    }
    return null;
  }

  @override
  Future<RecordSnapshot> indexGetSnapshot<K>(IndexRef<K> index, K key) async {
    var results = await sqfliteClient.query(index.store.name,
        where: '${index.column.name} = ?', whereArgs: [key]);
    if (results.isNotEmpty) {
      var row = results.first;
      return wrapSnapshotFromRow(row);
    }
    return null;
  }

  Future<T> inTransaction<T>(
      FutureOr<T> Function(DbClientSqfliteMixin txn) action) async {
    // TODO handle in transaction
    return await action(this);
  }

  @override
  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge}) async {
    content = await _put(record.store, record.key, content);
    return wrapSnapshot(record.key, content);
  }

  @override
  Future<ResultSet> getQueryResultSet(QueryRef query,
      {PageCursor cursor}) async {
    var builder = QueryBuilderSqflite(query.finder);
    if (cursor != null) {
      builder.pageCursor = cursor as PageCursorSqflite;
    }
    var results = await sqfliteClient.query(query.store.name,
        where: builder.where,
        whereArgs: builder.whereArgs,
        offset: builder.offset,
        limit: builder.limit);
    var list = <RecordSnapshotSqflite>[];
    RecordSnapshotSqflite lastSnapshot;
    for (var row in results) {
      lastSnapshot = wrapSnapshotFromRow(row);
      list.add(lastSnapshot);
    }
    var pageCursor = list.isNotEmpty
        ? PageCursorSqflite(snapshotSqflite: lastSnapshot)
        : null;
    return ResultSetImpl(snapshots: list, cursor: pageCursor);
  }
}
