const finderLimitDefault = 1000;

abstract class Finder {
  int get limit;

  int get offset;

  factory Finder({int limit, int offset}) =>
      FinderImpl(limit: limit, offset: offset);
}

class FinderImpl implements Finder {
  @override
  final int offset;

  int _limit;

  FinderImpl({this.offset, int limit}) {
    _limit = limit ?? finderLimitDefault;
  }

  FinderImpl cloneWithLimit({int limit}) {
    return FinderImpl(limit: limit);
  }

  @override
  int get limit => _limit;
}
