import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db_content.dart';
import 'package:tekartik_app_db/src/db_result_set.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/finder.dart';
import 'package:tekartik_app_db/src/index_query_ref.dart';
import 'package:tekartik_app_db/src/query_ref.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

abstract class DbClient {
  Future<int> add(StoreRef store, Map<String, dynamic> content);

  Future<RecordSnapshot> getSnapshot(RecordRef record);

  Future<RecordSnapshot> indexGetSnapshot<K>(IndexRef<K> indexRef, K key);

  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge});

  Future<ResultSet> getQueryResultSet(QueryRef query, {PageCursor cursor});
}
/*
/// Internal API
abstract class AppDbClient {
  Future<DbMetaDatabase>
}
*/

enum TransactionMode { readOnly, readWrite }

abstract class DbTransaction implements DbClient {}

/// Database object
abstract class Db implements DbClient {
  Future close();

  Future<T> getSnapshotOld<T extends DbSnapshot>(RecordRef record, {T content});

  Future putContent(DbContent context);

  Future clear(StoreRef store);

  Future<int> addContent(StoreRef store, DbContent content);

  Future<DbResultSet> getQuerySnapshotsOld(QueryRef query,
      {DbPageCursor cursor});

  Future<DbResultSet> getIndexQuerySnapshots<T>(IndexQueryRef<T> indexRef,
      {DbPageCursor cursor});

  /// By default the transaction is made on all the stores in read only mode
  Future<T> transaction<T>(FutureOr<T> Function(DbTransaction txn) action,
      {List<StoreRef> stores, TransactionMode mode});
}

/// Common extension
extension DbExt on Db {}

extension _DbClientExt on DbClient {
  Future<T> getDbSnapshot<T extends DbSnapshot>(DbRecord record) async {
    return recordToDbSnapshot(record.store, await getSnapshot(record.ref));
  }
}

T recordToDbSnapshot<T>(DbStore store, RecordSnapshot snapshot) {
  if (snapshot != null) {
    if (store.build == null) {
      var dbSnapshot = DbSnapshotImpl(snapshot.key, snapshot.value);
      return dbSnapshot as T;
    } else {
      var dbContent = store.build() as DbContent;
      dbContent.fromRecordSnapshot(snapshot);
      return dbContent as T;
    }
  }
  return null;
}

/// Common DbStore extension actions.
extension DbStoreExt<T extends DbSnapshot> on DbStore<T> {
  /// Add a record, (key must be null)
  Future<DbRecord<T>> add(DbClient dbClient, T content) async {
    assert(content.key == null);
    var key = await dbClient.add(ref, content.toMap());
    if (key == null) {
      return null;
    }
    return record(key);
  }
}

/// DbSchema helper
extension DbSchemaExt on DbSchema {
  Future<void> create(OpenTransaction transaction) async {
    for (var store in dbStores) {
      // ignore: unused_local_variable
      var txnStore = transaction.addStore(store.ref);
      for (var index in store.schema.indecies) {
        txnStore.addIndex(store.ref.index(column: index.column.column));
      }
    }
  }
}

extension DbIndexExt<T extends DbSnapshot, K> on DbIndex<T, K> {
  Future<T> get(DbClient dbClient, K key) async {
    var snapshot = await dbClient.indexGetSnapshot(ref, key);
    return recordToDbSnapshot(store, snapshot);
  }
}

/// Common extension
extension DbRecordExt<T extends DbSnapshot> on DbRecord<T> {
  /// Store with builder
  Future<T> get(DbClient dbClient) {
    return dbClient.getDbSnapshot(this);
  }

  Future<T> set(DbClient dbClient, T content, {bool merge}) async {
    assert(content.key != null);
    var snapshot = await dbClient.set(ref, content.toMap(), merge: merge);
    return ((store.build() as DbContent)..fromRecordSnapshot(snapshot)) as T;
  }
}

extension RecordExt on RecordRef {
  Future<T> getDbSnapshot<T extends DbSnapshot>(Db db, {T content}) =>
      db.getSnapshotOld(this, content: content);

  Future<RecordSnapshot> getSnapshot(DbClient dbClient) =>
      dbClient.getSnapshot(this);

  Future<Map<String, dynamic>> get(DbClient dbClient) async =>
      (await getSnapshot(dbClient))?.value;
}

extension StoreExt on StoreRef {
  Future<int> add(DbClient dbClient, Map<String, dynamic> content) =>
      dbClient.add(this, content);

  Future<int> addOld(Db db, DbContent content) => db.addContent(this, content);

  Future clear(Db db) => db.clear(this);
}

/*
sembast.SembastFinder _unwrapFinder(Finder finder) =>
    (finder as sembast.SembastFinder);
*/

Future<DbResultSet> _loopOld(
    Future<DbResultSet> Function(DbPageCursor cursor) page) async {
  DbPageCursor cursor;
  DbResultSet resultSet;
  while (true) {
    var pageResultSet = await page(cursor);
    if (resultSet == null) {
      resultSet = pageResultSet;
    } else {
      resultSet = DbResultSetImpl(
          done: pageResultSet.done,
          cursor: pageResultSet.cursor,
          snapshots: <DbSnapshot>[...resultSet, ...pageResultSet]);
    }
    if (pageResultSet.done) {
      break;
    }
    cursor = pageResultSet.cursor;
  }
  return resultSet;
}

Future<ResultSet> _loop(
    Future<ResultSet> Function(PageCursor cursor) page) async {
  PageCursor cursor;
  ResultSet resultSet;
  while (true) {
    var pageResultSet = await page(cursor);
    if (resultSet == null) {
      resultSet = pageResultSet;
    } else {
      resultSet = ResultSetImpl(
          cursor: pageResultSet.cursor,
          snapshots: <RecordSnapshot>[...resultSet, ...pageResultSet]);
    }
    if (pageResultSet.cursor == null || pageResultSet.isEmpty) {
      break;
    }
    cursor = pageResultSet.cursor;
  }
  return resultSet;
}

extension IndexQueryExt<T> on IndexQueryRef<T> {
  // Old but nice with flat
  Future<DbResultSet> get(Db db, {DbPageCursor cursor}) {
    return db.getIndexQuerySnapshots<T>(this, cursor: cursor);
  }

  Future<DbSnapshot> first(Db db, T key) async {
    // var sembastFinder = _unwrapFinder(finder);
    var finder = this.finder as FinderImpl;
    finder = finderCloneWithLimit(finder, limit: 1);
    var query = IndexQueryRefImpl<T>(index: index, finder: finder, key: key);
    var results = await IndexQueryExt<T>(query).get(db);
    return results.firstWhere((_) => true, orElse: () => null);
  }

  /// Fetch up to the limit.
  Future<DbResultSet> all(Db db) async =>
      _loopOld((cursor) async => await get(db, cursor: cursor));
}

FinderImpl finderCloneWithLimit(Finder finder, {@required int limit}) {
  var finderImpl = finder as FinderImpl;
  if (finder?.limit != 1) {
    finderImpl = FinderImpl(offset: finder?.offset, limit: limit);
  }
  return finderImpl;
}

extension QueryExt on QueryRef {
  Future<ResultSet> get(DbClient db, {PageCursor cursor}) =>
      db.getQueryResultSet(this, cursor: cursor);

  Future<DbResultSet> getOld(Db db, {DbPageCursor cursor}) =>
      db.getQuerySnapshotsOld(this, cursor: cursor);

  /// Fetch up to the limit.
  Future<ResultSet> all(Db db) async =>
      _loop((cursor) async => await get(db, cursor: cursor));

  /// Fetch up to the limit.
  Future<DbResultSet> allOld(Db db) async =>
      _loopOld((cursor) async => await getOld(db, cursor: cursor));

  Future<DbSnapshot> first(Db db) async {
    // var sembastFinder = _unwrapFinder(finder);
    var finder = this.finder as FinderImpl;
    finder = finderCloneWithLimit(finder, limit: 1);

    var query = QueryRefImpl(store: store, finder: finder);
    var results = await QueryExt(query).getOld(db);
    return results.firstWhere((_) => true, orElse: () => null);
    //if (results.)
    // db.getQuerySnapshots(store.query(finder:sembastFinder )limit(1));
  }
}
