import 'package:tekartik_app_db/app_db.dart';

/// Snapshot minimum interface
abstract class RecordSnapshot {
  int get key;
  Map<String, dynamic> get value;

  /// Create a snapshot
  factory RecordSnapshot(int key, Map<String, dynamic> value) =>
      RecordSnapshotImpl(key, value);
}

class RecordSnapshotImpl implements RecordSnapshot {
  @override
  final int /*!*/ key;

  @override
  final Map<String, dynamic> /*!*/ value;

  RecordSnapshotImpl(this.key, this.value);

  @override
  String toString() => '$key $value';
}

extension RecordSnapshotDev on RecordSnapshot {
  Model toDebugMap() {
    var map = Model(Map.from(value));
    map['_id'] = key;
    return map;
  }
}
