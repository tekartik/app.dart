import 'package:meta/meta.dart';

///
/// Sort order boundary, lower or upper to use in a [Finder]
///
abstract class Boundary<T> {
  final T value;

  /// if true, the boundary will be included in the search result.
  ///
  /// defaults to false.
  bool get include;

  /// Create a boundary from a set of [values] or a given [record]
  ///
  /// if [include] is true, the record at the boundary will be included
  /// Number of values should match the number or sort orders
  ///
  /// [snapshot] superseeds record
  ///
  factory Boundary({T value, bool include}) {
    return BoundaryImpl<T>(value: value, include: include);
  }
}

class BoundaryImpl<T> implements Boundary<T> {
  @override
  final bool include;

  @override
  final T value;

  BoundaryImpl({@required bool include, @required this.value})
      : include = include ?? false;
}
