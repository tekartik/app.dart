import 'package:meta/meta.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db_index.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/store_ref.dart';

/// custom object must extends DbContent
class DbStore<T extends DbSnapshot> {
  final T Function() build;
  final List<DbIndex> indecies = <DbIndex>[];
  final StoreRef ref;

  DbStore(this.ref, this.build);

  String get name => ref.name;

  /// Create a model (actually an object)
  T get model => build();

  /// Store with builder
  DbRecord<T> record(int key) {
    return DbRecord<T>(this, key);
  }

  DbStoreSchema get schema {
    return DbStoreSchema(
        name: name,
        indecies: indecies.map((dbIndex) => dbIndex.schema).toList());
  }

  /// Create an index ref
  DbIndex<T, K> index<K>({@required Column<K> column}) {
    return DbIndex(this, column);
  }
}

class DbStoreSchema {
  final String name;
  final List<DbIndexSchema> indecies;
  final List<DbColumnSchema> columns;

  DbStoreSchema({@required this.name, this.indecies, this.columns});
}
