import 'package:sembast/sembast.dart' as sembast;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:tekartik_app_db/src/sembast/sembast_db.dart';

mixin DbClientSembastMixin implements DbClient {
  sembast.DatabaseClient get sembastClient;

  @override
  Future<int> add(StoreRef store, Map<String, dynamic> content) {
    return unwrapStore(store).add(sembastClient, content);
  }

  @override
  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge}) {
    // TODO: implement set
    throw UnimplementedError();
  }

  @override
  Future<RecordSnapshot> getSnapshot(RecordRef record) async {
    var sembastSnapshot = await unwrapRecord(record).getSnapshot(sembastClient);
    return wrapSnapshot(sembastSnapshot);
  }

  @override
  Future<RecordSnapshot> indexGetSnapshot<K>(IndexRef<K> index, K key) async {
    var sembastStore = unwrapStore(index.store);
    var sembastSnapshot = await sembastStore
        .query(
            finder: sembast.Finder(
                filter: sembast.Filter.equals(index.column.name, key)))
        .getSnapshot(sembastClient);
    return wrapSnapshot(sembastSnapshot);
  }

  @override
  Future<ResultSet> getQueryResultSet(QueryRef query,
      {PageCursor cursor}) async {
    var sembastStore = unwrapStore(query.store);
    RecordSnapshotSembast lastSnapshot;
    sembast.QueryRef sembastQuery;

    var finderBuilder = StoreFinderBuilder(query.finder);
    if (cursor != null) {
      lastSnapshot = (cursor as PageCursorSembast).lastSnapshot;

      if (lastSnapshot != null) {
        finderBuilder.preFilter(
            sembast.Filter.greaterThan(sembast.Field.key, lastSnapshot.key));
      }
      // remove offset
      finderBuilder.offset = 0;
    }

    // Build sembast query
    sembastQuery = sembastStore.query(finder: finderBuilder.toSembastFinder());

    // Perform the query
    var sembastSnapshots = await sembastQuery.getSnapshots(sembastClient);

    var snapshots = sembastSnapshots
        .map<RecordSnapshot>((sembastSnapshot) => wrapSnapshot(sembastSnapshot))
        .toList(growable: false);
    lastSnapshot = snapshots.lastWhere((_) => true, orElse: () => null)
        as RecordSnapshotSembast;
    return ResultSetImpl(
        cursor: lastSnapshot != null ? PageCursorSembast(lastSnapshot) : null,
        snapshots: snapshots);
  }
}
