import 'package:sembast/sembast.dart' as sembast;
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/sembast/sembast_client.dart';

class TransactionSembast with DbClientSembastMixin implements DbTransaction {
  final sembast.Transaction sembastTransaction;
  final List<StoreRef> stores;

  TransactionSembast({this.stores, this.sembastTransaction});

  @override
  sembast.DatabaseClient get sembastClient => sembastTransaction;
}
