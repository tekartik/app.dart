import 'package:tekartik_app_db/app_db.dart';
import 'package:sembast/sembast_memory.dart';
import 'package:sembast/sembast.dart' as sembast;
import 'package:tekartik_app_db/src/sembast/sembast_db.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

class DatabaseProviderSembast implements DatabaseProvider {
  final sembast.DatabaseFactory databaseFactory;

  DatabaseProviderSembast({@required this.databaseFactory});
  @override
  Future<Db> openDatabase(String name, {DatabaseOptions options}) async {
    var sembastDb = await databaseFactory.openDatabase(name,
        version: options?.version ?? 1,
        onVersionChanged: (txn, oldVersion, newVersion) {});
    return DbSembast(databaseProvider: this, sembastDatabase: sembastDb);
  }

  @override
  Future deleteDatabase(String name) async {
    await databaseFactory.deleteDatabase(name);
  }

  Future closeDatabase(Db db) async {
    var dbSembast = db as DbSembast;
    await dbSembast.sembastDatabase.close();
  }
}

final inMemoryDatabaseProvider =
    DatabaseProviderSembast(databaseFactory: databaseFactoryMemory);

DatabaseProvider newDatabaseProviderMemory() =>
    DatabaseProviderSembast(databaseFactory: databaseFactoryMemory);

DatabaseProvider getDatabaseProviderSembast(
        {sembast.DatabaseFactory sembastDatabaseFactory}) =>
    DatabaseProviderSembast(databaseFactory: sembastDatabaseFactory);
