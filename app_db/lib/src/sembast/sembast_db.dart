import 'package:sembast/sembast.dart' as sembast;
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/boundary.dart';
import 'package:tekartik_app_db/src/db.dart';
import 'package:tekartik_app_db/src/db_content.dart';
import 'package:tekartik_app_db/src/db_result_set.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/finder.dart';
import 'package:tekartik_app_db/src/query_ref.dart';
import 'package:tekartik_app_db/src/sembast/sembast_client.dart';
import 'package:tekartik_app_db/src/sembast/sembast_provider.dart';
import 'package:tekartik_app_db/src/sembast/sembast_transaction.dart';
import 'package:tekartik_app_db/src/tuple_column.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

import 'sembast_import.dart' show SembastFinder;

/*
Store _wrapStore(sembast.StoreRef<int, Map<String, dynamic>> storeRef) {
  return Store(name: storeRef.name);
}
*/

sembast.StoreRef<int, Map<String, dynamic>> unwrapStore(StoreRef store) =>
    sembast.intMapStoreFactory.store(store.name);

sembast.RecordRef<int, Map<String, dynamic>> unwrapRecord(RecordRef record) =>
    unwrapStore(record.store).record(record.key);

/*
sembast.RecordRef<int, Map<String, dynamic>> _unwrapQuery(QueryRef queryRef) =>
    _unwrapStore(record.store).record(record.key);

 */

class FinderSembast {
  final sembast.Finder sembastFinder;

  FinderSembast(this.sembastFinder);
}

class RecordSnapshotSembast implements RecordSnapshot {
  final sembast.RecordSnapshot<int, Map<String, dynamic>> _snapshot;

  RecordSnapshotSembast(this._snapshot);

  @override
  int get key => _snapshot.key;

  @override
  Map<String, dynamic> get value => _snapshot.value;

  @override
  String toString() => '$key $value';
}

/// Slow implementation, use custom type instead.
class DbSnapshotSembast
    with DbSnapshotMixin, ContentMixin, CvModelBaseMixin
    implements DbSnapshot {
  final sembast.RecordSnapshot<int, Map<String, dynamic>> _snapshot;

  List<Field> _fields;
  Map<String, Field> _fieldMap;

  DbSnapshotSembast(this._snapshot);

  @override
  List<Field> get fields => _fields ??= () {
        _snapshot.value?.forEach((key, value) {
          _fieldMap ??= <String, Field>{};
          _fieldMap[key] = Field(key)..v = value;
        });
        return _fieldMap?.values?.toList(growable: false);
      }();

  @override
  int get key => _snapshot.key;

  @override
  Field<T> field<T>(String name) {
    // create map if needed
    fields;
    return _fieldMap[name]?.cast<T>();
  }
}

DbSnapshotSembast _wrapSnapshotOld(sembast.RecordSnapshot snapshot) {
  if (snapshot == null) {
    return null;
  } else {
    return DbSnapshotSembast(snapshot.cast<int, Map<String, dynamic>>());
  }
}

RecordSnapshotSembast wrapSnapshot(sembast.RecordSnapshot snapshot) {
  if (snapshot == null) {
    return null;
  } else {
    return RecordSnapshotSembast(snapshot.cast<int, Map<String, dynamic>>());
  }
}

abstract class FinderBuilder {
  int limit;
  int offset;
  final sortOrders = <sembast.SortOrder>[];

  FinderBuilder([Finder finder]) {
    if (finder != null) {
      limit = finder.limit;
      offset = finder.offset;
    }
  }

  sembast.Boundary _start;
  sembast.Boundary _end;

  sembast.Filter sembastFilter;

  // String get key => sembast.Field.key;

  void preFilter(sembast.Filter filter) {
    if (sembastFilter == null) {
      sembastFilter = filter;
    } else {
      sembastFilter = sembast.Filter.and([filter, sembastFilter]);
    }
  }

  sembast.Finder toSembastFinder() {
    return (sembast.Finder(
        limit: limit,
        offset: offset,
        filter: sembastFilter,
        sortOrders: sortOrders,
        start: _start,
        end: _end)) as SembastFinder;
  }

  // Don't include last primary key
  set pageCursor(DbPageCursorSembast pageCursor) {
    _start =
        sembast.Boundary(values: [pageCursor.lastSnapshot.key], include: false);
  }
}

class StoreFinderBuilder extends FinderBuilder {
  StoreFinderBuilder([Finder finder]) : super(finder) {
    sortOrders.add(sembast.SortOrder(sembast.Field.key));
  }
}

class IndexFinderBuilder extends FinderBuilder {
  final IndexQueryRef indexQuery;

  void initOrders({bool forCursor}) {
    sortOrders.clear();
    for (var column in columnsAsList(indexQuery.column)) {
      sortOrders.add(sembast.SortOrder(column.name));
    }
    if (forCursor ?? false) {
      sortOrders.add(sembast.SortOrder(sembast.Field.key));
    }
  }

  IndexFinderBuilder(this.indexQuery) : super(indexQuery.finder) {
    initOrders();
  }

  // @override
  String get keyPath => indexQuery.column.name;

  set startBoundary(Boundary boundary) {
    initOrders();
    _start = sembast.Boundary(values: [
      ...tupleValues(boundary.value),
    ], include: boundary.include);
  }

  set endBoundary(Boundary boundary) {
    initOrders();
    _end = sembast.Boundary(values: [
      ...tupleValues(boundary.value),
    ], include: boundary.include);
  }

  set key(dynamic key) {
    initOrders();
    if (indexQuery.column is TupleColumn) {
      var tupleColumn = indexQuery.column as TupleColumn;
      var columns = tupleColumn.columns;
      var filters = <sembast.Filter>[];
      var keyValues = tupleValues(key);
      for (var i = 0; i < columns.length; i++) {
        var column = columns[i];
        var value = keyValues[i];
        filters.add(sembast.Filter.equals(column.name, value));
      }
      preFilter(sembast.Filter.and(filters));
    } else {
      preFilter(sembast.Filter.equals(keyPath, key));
    }
  }

  // Always include and filter out later
  @override
  set pageCursor(DbPageCursorSembast pageCursor) {
    initOrders(forCursor: true);
    _start = sembast.Boundary(values: [
      ...columnsAsList(indexQuery.column)
          .map((column) => pageCursor.lastSnapshot.field(column.name)?.v),
      pageCursor.lastSnapshot.key
    ], include: false);

    // Remove offset
    offset = 0;
  }
}

class DbPageCursorSembast implements DbPageCursor {
  final DbSnapshotSembast lastSnapshot;

  DbPageCursorSembast(this.lastSnapshot);

  @override
  String toString() => 'Cursor(last: $lastSnapshot)';
}

class PageCursorSembast implements PageCursor {
  final RecordSnapshotSembast lastSnapshot;

  PageCursorSembast(this.lastSnapshot);

  @override
  String toString() => 'Cursor(last: $lastSnapshot)';
}

class DbResultSetSembast<T extends DbSnapshot> extends DbResultSetImpl<T> {}

class DbSembast with DbClientSembastMixin implements Db {
  final DatabaseProviderSembast databaseProvider;
  final sembast.Database sembastDatabase;

  DbSembast({@required this.databaseProvider, @required this.sembastDatabase});

  @override
  Future close() async {
    await databaseProvider.closeDatabase(this);
  }

  @override
  Future<T> getSnapshotOld<T extends DbSnapshot>(RecordRef record,
      {T content}) async {
    var sembastSnapshot =
        await unwrapRecord(record).getSnapshot(sembastDatabase);
    if (content == null) {
      return _wrapSnapshotOld(sembastSnapshot) as T;
    } else {
      if (sembastSnapshot == null) {
        return null;
      }
      var dbContent = content as DbContent;
      dbContent.fromMap(sembastSnapshot.value);
      dbContent.key = sembastSnapshot.key;
      return content;
    }
  }

  @override
  Future putContent(DbContent context) {
    // TODO: implement putSnapshot
    throw UnimplementedError();
  }

  @override
  Future<DbResultSet> getQuerySnapshotsOld(QueryRef query,
      {DbPageCursor cursor}) async {
    var sembastStore = unwrapStore(query.store);
    DbSnapshotSembast lastSnapshot;
    sembast.QueryRef sembastQuery;

    var finderBuilder = StoreFinderBuilder(query.finder);
    if (cursor != null) {
      lastSnapshot = (cursor as DbPageCursorSembast).lastSnapshot;

      if (lastSnapshot != null) {
        finderBuilder.preFilter(
            sembast.Filter.greaterThan(sembast.Field.key, lastSnapshot.key));
      }
      // remove offset
      finderBuilder.offset = 0;
    }

    // Build sembast query
    sembastQuery = sembastStore.query(finder: finderBuilder.toSembastFinder());

    // Perform the query
    var sembastSnapshots = await sembastQuery.getSnapshots(sembastDatabase);

    var snapshots = sembastSnapshots
        .map<DbSnapshot>((sembastSnapshot) => _wrapSnapshotOld(sembastSnapshot))
        .toList(growable: false);
    lastSnapshot = snapshots.lastWhere((_) => true, orElse: () => null)
        as DbSnapshotSembast;
    return DbResultSetImpl(
        // nothing found
        done: lastSnapshot == null,
        cursor: lastSnapshot != null ? DbPageCursorSembast(lastSnapshot) : null,
        snapshots: snapshots);
  }

  @override
  Future<int> addContent(StoreRef store, DbContent content) async {
    return await unwrapStore(store).add(sembastDatabase, content.toMap());
  }

  @override
  Future<DbResultSet<DbSnapshot>> getIndexQuerySnapshots<T>(
      IndexQueryRef<T> index,
      {DbPageCursor cursor}) async {
    var sembastStore = unwrapStore(index.store);
    DbSnapshotSembast lastSnapshot;
    sembast.QueryRef sembastQuery;

    var startBoundary = index.start;
    var endBoundary = index.end;
    var finderBuilder = IndexFinderBuilder(index);

    if (cursor != null) {
      finderBuilder.pageCursor = (cursor as DbPageCursorSembast);
    } else {
      if (index.key != null) {
        finderBuilder.key = index.key;
      } else {
        if (startBoundary != null) {
          finderBuilder.startBoundary = startBoundary;
        }
      }
    }
    if (endBoundary != null) {
      finderBuilder.endBoundary = endBoundary;
    }

    // Build sembast query
    sembastQuery = sembastStore.query(finder: finderBuilder.toSembastFinder());

    // Perform the query
    var sembastSnapshots = await sembastQuery.getSnapshots(sembastDatabase);

    var snapshots = sembastSnapshots
        .map<DbSnapshot>((sembastSnapshot) => _wrapSnapshotOld(sembastSnapshot))
        .toList(growable: false);
    lastSnapshot = snapshots.lastWhere((_) => true, orElse: () => null)
        as DbSnapshotSembast;
    return DbResultSetImpl(
        // nothing found
        done: lastSnapshot == null,
        cursor: lastSnapshot != null ? DbPageCursorSembast(lastSnapshot) : null,
        snapshots: snapshots);
  }

  @override
  Future clear(StoreRef store) async {
    var sembastStore = unwrapStore(store);
    await sembastStore.delete(sembastDatabase);
  }

  @override
  Future<int> add(StoreRef store, Map<String, dynamic> content) async {
    return await unwrapStore(store).add(sembastDatabase, content);
  }

  @override
  Future<RecordSnapshot> set(RecordRef record, Map<String, dynamic> content,
      {bool merge}) async {
    var map =
        await unwrapRecord(record).put(sembastDatabase, content, merge: merge);
    if (map != null) {
      return RecordSnapshot(record.key, map);
    } else {
      return null;
    }
  }

  @override
  Future<T> transaction<T>(FutureOr<T> Function(DbTransaction txn) action,
      {List<StoreRef> stores, TransactionMode mode}) async {
    return sembastDatabase.transaction((transaction) async {
      var txn =
          TransactionSembast(stores: stores, sembastTransaction: transaction);
      var result = await action(txn);
      return result;
    });
  }

  @override
  sembast.DatabaseClient get sembastClient => sembastDatabase;
}
