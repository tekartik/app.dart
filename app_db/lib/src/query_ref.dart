import 'package:meta/meta.dart';
import 'package:tekartik_app_db/app_db.dart';

abstract class QueryRef {
  StoreRef get store;

  Finder get finder;
}

class QueryRefImpl implements QueryRef {
  @override
  final StoreRef store;

  @override
  final Finder finder;

  QueryRefImpl({@required this.store, this.finder});
}
