import 'package:tekartik_common_utils/model/model.dart';

class StoreMeta {
  String name;

  Model toMap() {
    var map = Model();
    return map;
  }
}

class DbMeta {
  int version;
  // List<StoreMeta> stores;
  final _storeMap = <String, StoreMeta>{};

  void fromMap(Map map) {}

  void addStoreMeta(StoreMeta storeMeta) {
    //stores.add(storeMeta);
    _storeMap[storeMeta.name] = storeMeta;
  }

  Model toMap() {
    var map = Model();
    var stores = <Model>[];
    var storeKeys = List<String>.from(_storeMap.keys)..sort();
    for (var storeKey in storeKeys) {
      stores.add(_storeMap[storeKey].toMap());
    }
    if (stores.isNotEmpty) {
      map['stores'] = stores;
    }
    return map;
  }
}
