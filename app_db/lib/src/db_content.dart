import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/db_snapshot.dart';
import 'package:tekartik_app_db/src/utils.dart';

/// Declaring indexed
abstract class ContentWithIndexedFields {
  List<Field> get indexedFields;
}

abstract class DbContent extends DbSnapshot
    with DbSnapshotMixin, ContentMixin, CvModelBaseMixin
    implements DbModel {
  /// to override something like [name, description]
  @override
  List<Field> get fields;

  /// The record key
  @override
  int key;

  /*
  @override
  void fromMap(Map map, {List<Field> fields, int id}) {
    _key = id;
    fields ??= this.fields;
    var model = Model(map);
    for (var field in fields) {
      var entry = model.getModelEntry(field.name);
      if (entry != null) {
        field.v = entry.value;
      }
    }
  }
  */
  void fromSnapshot(DbSnapshot snapshot) {
    fromContent(snapshot);
    key = snapshot.key;
  }

  @override
  String toString() {
    try {
      return logTruncate('[$key] ${toMap()}');
    } catch (e) {
      return logTruncate('[$key] ${fields}');
    }
  }

  @override
  bool operator ==(other) {
    if (other is DbSnapshot) {
      // Check key first
      if (other.key != key) {
        return false;
      }
      return Content.equals(this, other);
    }
    return super == other;
  }

  void fromRecordSnapshot(RecordSnapshot snapshot) {
    key = snapshot.key;
    for (var field in fields) {
      var value = snapshot.value[field.name];
      if (value == null) {
        // Set or not?
        if (snapshot.value.containsKey(field.name)) {
          field.setNull();
        }
      } else {
        field.setValue(value);
      }
    }
  }

  @override
  int get hashCode => (key ?? 0) + super.hashCode;
}
