import 'package:meta/meta.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/boundary.dart';
import 'package:tekartik_app_db/src/index_query_ref.dart';

/// A simple index
abstract class IndexRef<T> {
  StoreRef get store;
  Column<T> get column;

  // Index name
  String get name;

  IndexQueryRef<T> query(
      {Finder finder, Boundary<T> start, Boundary<T> end, T key});
}

class IndexRefImpl<T> implements IndexRef<T> {
  @override
  final StoreRef store;

  @override
  final Column<T> column;

  IndexRefImpl({
    @required this.store,
    @required this.column,
  }) {
    assert(store != null);
    assert(column != null);
  }

  /*
  @override
  IndexQueryRef<T> query({Finder finder}) =>
      IndexQueryRefImpl<T>(index: this, finder: finder);

   */

  @override
  String toString() => 'Index($store, $column)';

  @override
  int get hashCode => store.hashCode + column.hashCode;

  @override
  bool operator ==(other) {
    if (other is IndexRefImpl) {
      if (other.store != store) {
        return false;
      }
      if (other.column != column) {
        return false;
      }
      return true;
    }
    return false;
  }

  @override
  IndexQueryRef<T> query(
          {Finder finder, Boundary<T> start, Boundary<T> end, T key}) =>
      IndexQueryRefImpl<T>(
          index: this, finder: finder, start: start, end: end, key: key);

  @override
  String get name => column.name;
}
