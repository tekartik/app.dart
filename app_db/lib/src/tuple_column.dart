import 'package:tekartik_app_db/app_db.dart';
import 'package:tuple/tuple.dart';

abstract class TupleColumn {
  List rawValuesAll(List values);
  List<Column> get columns;
}

mixin TupleColumnMixin implements TupleColumn {}

/// Requires type Tuple
class TupleColumn2<T1, T2>
    with TupleColumnMixin, ColumnMixin<Tuple2<T1, T2>>, ColumnNameMixin
    implements Column<Tuple2<T1, T2>>, TupleColumn {
  final List<Column> _columns;

  TupleColumn2(this._columns) {
    name = _columns.map((column) => column.name).join(',');
  }

  TupleColumn2.fromColumns(Column<T1> column1, Column<T2> column2)
      : this([column1, column2]);

  Tuple2<T1, T2> values(T1 value1, T2 value2) => Tuple2(value1, value2);

  Tuple2<T1, T2> valuesAll(List values) => Tuple2.fromList(values);
  @override
  List rawValuesAll(List values) => valuesAll(values).toList(growable: false);

  @override
  List<Column> get columns => _columns;
}

/// Tuple or not values as list.
List tupleValues(dynamic value) {
  if (value is Tuple2) {
    return value.toList();
  } else if (value is Tuple3) {
    return value.toList();
  } else if (value is Tuple4) {
    return value.toList();
  } else if (value is Tuple5) {
    return value.toList();
  } else if (value is Tuple6) {
    return value.toList();
  } else if (value is Tuple7) {
    return value.toList();
  }
  return [value];
}

/// List for tuple, simple value otherwise.
dynamic tupleValuesOrValue(dynamic value) {
  if (value is Tuple2) {
    return value.toList();
  } else if (value is Tuple3) {
    return value.toList();
  } else if (value is Tuple4) {
    return value.toList();
  } else if (value is Tuple5) {
    return value.toList();
  } else if (value is Tuple6) {
    return value.toList();
  } else if (value is Tuple7) {
    return value.toList();
  }
  return value;
}

/// List of columns
List<Column> columnsAsList(Column column) =>
    column is TupleColumn ? (column as TupleColumn).columns : [column];

/// handle tuple
dynamic columnsNamesOrName(Column column) => column is TupleColumn
    ? (column as TupleColumn).columns.map((column) => column.name).toList()
    : column.name;

/// handle tuple
List<String> columnsNames(Column column) => column is TupleColumn
    ? (column as TupleColumn).columns.map((column) => column.name).toList()
    : [column.name];

/// List or single value
dynamic /*List<Column> | Column*/ columnsAsListOrSingle(Column column) =>
    column is TupleColumn ? (column as TupleColumn).columns : [column];
