import 'package:meta/meta.dart';
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/src/boundary.dart';

abstract class IndexQueryRef<T> {
  IndexRef<T> get index;
  Boundary<T> get start;
  Boundary<T> get end;
  T get key;
  Finder get finder;

  StoreRef get store;
  Column<T> get column;
}

class IndexQueryRefImpl<T> implements IndexQueryRef<T> {
  @override
  final T key;
  @override
  final Finder finder;
  /*
  @override
  StoreRef get store => index.store;

  final T key;
  @override
  final Finder finder;
  */
  IndexQueryRefImpl(
      {@required this.index, this.key, this.start, this.end, this.finder});

  @override
  final IndexRef<T> index;

  @override
  final Boundary<T> end;

  @override
  final Boundary<T> start;

  @override
  StoreRef get store => index.store;

  @override
  Column<T> get column => index.column;
}
