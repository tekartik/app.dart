import 'package:tekartik_app_content/cv.dart';
import 'package:test/test.dart';

import 'content_test.dart';

void main() {
  group('CvMapModel', () {
    test('fromMap', () {
      var cv = CvMapModel();
      cv['test'] = 1;
      expect(cv.toModel(), {'test': 1});
      cv = CvMapModel()..fromModel({'test': 1});
      expect(cv.toModel(), {'test': 1});
      cv.fromModel({'test': 2});
      expect(cv.toModel(), {'test': 2});
    });
    test('toMap', () {
      var cv = CvMapModel();
      cv['test'] = 1;
      expect(cv.toMap(columns: ['test']), {'test': 1});
      expect(cv.toMap(columns: []), {});
    });
    test('child content toMap', () {
      var cv = CvMapModel();
      cv['test'] = ChildContent()..sub.v = 'sub_v';
      expect(cv.toMap(columns: ['test']), {
        'test': {'sub': 'sub_v'}
      });
      expect(cv.toMap(columns: []), {});
    });
    test('child content list toMap', () {
      var cv = CvMapModel();
      cv['test'] = [ChildContent()..sub.v = 'sub_v'];
      expect(cv.toMap(columns: ['test']), {
        'test': [
          {'sub': 'sub_v'}
        ]
      });
      expect(cv.toMap(columns: []), {});
    });
    test('map', () {
      var cv = CvMapModel();
      expect(cv.fields, []);
      cv['test'] = 1;
      expect(cv.fields, [CvField('test', 1)]);
      cv['test'] = null;
      expect(cv.fields, [CvField('test')]);
      expect(cv.toMap(), {'test': null});
      cv.field('test').v = 2;
      expect(cv.fields, [CvField('test', 2)]);
      expect(cv.toMap(), {'test': 2});
      cv.field('test').clear();
      expect(cv.fields, []);
      expect(cv.toMap(), {});
    });
    test('fromMap', () async {
      try {
        IntContent().fromMap(null);
        fail('should have failed');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
      var content = IntContent()..fromMap({});
      expect(content.value.hasValue, isFalse);
    });
  });
}
