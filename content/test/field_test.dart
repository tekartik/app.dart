import 'package:tekartik_app_content/content.dart';
import 'package:test/test.dart';

void main() {
  group('Field', () {
    test('equals', () {
      expect(Field('name'), Field('name'));
      expect(Field('name'), Field('name', null));
      expect(Field('name', 1), Field('name', 1));
      expect(Field('name', 1), isNot(Field('name', 2)));
      expect(Field('name'), isNot(Field('name2')));
      expect(Field('name'), isNot(Field('name', 1)));
      try {
        Field(null);
        fail('should fail');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
    });

    test('fromField', () {
      expect(Field<String>('name')..fromField(Field('name', 'value')),
          Field('name', 'value'));
    });

    test('hasValue', () {
      var field = Field('name');
      expect(field.hasValue, isFalse);
      expect(field.v, isNull);
      field.setNull();
      expect(field.hasValue, isTrue);
      expect(field.v, isNull);
      field.clear();
      expect(field.hasValue, isFalse);
      expect(field.v, isNull);
    });
  });
}
