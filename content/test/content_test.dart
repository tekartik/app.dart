import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/src/field.dart';
import 'package:test/test.dart';

class Note extends ContentBase {
  final title = stringField('title');
  final content = stringField('content');
  final date = intField('date');

  @override
  List<Field> get fields => [title, content, date];
}

class IntContent extends ContentBase {
  final value = intField('value');

  @override
  List<Field> get fields => [value];
}

void main() {
  group('Content', () {
    test('equals', () {
      expect(IntContent(), IntContent());
      expect(IntContent()..value.v = 1, IntContent()..value.v = 1);
      expect(IntContent(), isNot(IntContent()..value.v = 1));
      expect(IntContent()..value.v = 2, isNot(IntContent()..value.v = 1));
    });
    test('toMap', () async {
      expect(IntContent().toMap(), {});
      expect((IntContent()..value.v = 1).toMap(), {'value': 1});
      expect((IntContent()..value.v = 1).toMap(columns: <String>[]), {});
      expect((IntContent()..value.v = 1).toMap(columns: [IntContent().value.k]),
          {'value': 1});
    });
    test('fromMap', () async {
      try {
        IntContent().fromMap(null);
        fail('should have failed');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
      expect(IntContent()..fromMap({}), IntContent());
      expect(IntContent()..fromMap({'value': 1}), IntContent()..value.v = 1);
      expect(
          IntContent()
            ..fromMap({'value': 1}, columns: [IntContent().value.name]),
          IntContent()..value.v = 1);
      expect(IntContent()..fromMap({'value': 1}, columns: []), IntContent());
    });
    test('allToMap', () async {
      var note = Note()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1;
      expect(note.toMap(),
          {'title': 'my_title', 'content': 'my_content', 'date': 1});
      expect(note.toMap(columns: [note.title.name]), {'title': 'my_title'});
    });
    test('duplicated field', () {
      try {
        WithDuplicatedFields().toMap();
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
      try {
        WithDuplicatedFields().fromMap({});
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
      try {
        WithDuplicatedFields().fromContent(ContentValues());
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
    });
    test('content child', () {
      var parent = WithChildField()
        ..child.v = (ChildContent()..sub.v = 'sub_value');
      var map = {
        'child': {'sub': 'sub_value'}
      };
      expect(parent.toMap(), map);
      parent = WithChildField()..fromMap(map);
      expect(parent.toMap(), map);
    });
    test('content child list', () {
      var parent = WithChildListField()
        ..children.v = [ChildContent()..sub.v = 'sub_value'];
      var map = {
        'children': [
          {'sub': 'sub_value'}
        ]
      };
      expect(parent.children.v.first.sub.v, 'sub_value');
      expect(parent.toMap(), map);
      parent = WithChildListField()..fromMap(map);
      expect(parent.toMap(), map);
    });
  });
}

class WithDuplicatedFields extends ContentBase {
  final field1 = stringField('field1');
  final field2 = stringField('field1');

  @override
  List<Field> get fields => [field1, field2];
}

class WithChildField extends ContentBase {
  final child = FieldContent<ChildContent>('child', (_) => ChildContent());

  @override
  List<Field> get fields => [child];
}

class WithChildListField extends ContentBase {
  final children =
      FieldContentList<ChildContent>('children', (_) => ChildContent());

  @override
  List<Field> get fields => [children];
}

class ChildContent extends ContentBase {
  final sub = stringField('sub');
  @override
  List<Field> get fields => [sub];
}
