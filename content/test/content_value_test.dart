import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/src/content_values.dart';

import 'package:test/test.dart';

import 'content_test.dart';

class Note extends CvBase {
  final title = stringField('title');
  final content = stringField('content');
  final date = Field<DateTime>('date');

  @override
  List<Field> get fields => [title, content, date];
}

class WithDate extends CvBase {
  final date = Field<DateTime>('date');

  @override
  List<Field> get fields => [date];
}

class IntContent extends CvBase {
  final value = intField('value');

  @override
  List<Field> get fields => [value];
}

void main() {
  group('ContentValues', () {
    test('fromMap', () {
      var cv = ContentValues();
      cv['test'] = 1;
      expect(cv.toMap(), {'test': 1});
      cv = ContentValues()..fromMap({'test': 1});
      expect(cv.toMap(), {'test': 1});
      cv.fromMap({'test': 2}, columns: []);
      expect(cv.toMap(), {'test': 1});
      cv.fromMap({'test': 2}, columns: [Column<int>('test').name]);
      expect(cv.toMap(), {'test': 2});
    });
    test('toMap', () {
      var cv = ContentValues();
      cv['test'] = 1;
      expect(cv.toMap(columns: ['test']), {'test': 1});
      expect(cv.toMap(columns: []), {});
    });
    test('child content toMap', () {
      var cv = ContentValues();
      cv['test'] = ChildContent()..sub.v = 'sub_v';
      expect(cv.toMap(columns: ['test']), {
        'test': {'sub': 'sub_v'}
      });
      expect(cv.toMap(columns: []), {});
    });
    test('child content list toMap', () {
      var cv = ContentValues();
      cv['test'] = [ChildContent()..sub.v = 'sub_v'];
      expect(cv.toMap(columns: ['test']), {
        'test': [
          {'sub': 'sub_v'}
        ]
      });
      expect(cv.toMap(columns: []), {});
    });
    test('map', () {
      var cv = ContentValues();
      expect(cv.fields, []);
      cv['test'] = 1;
      expect(cv.fields, [Field('test', 1)]);
      cv['test'] = null;
      expect(cv.fields, [Field('test')]);
      expect(cv.toMap(), {'test': null});
      cv.field('test').v = 2;
      expect(cv.fields, [Field('test', 2)]);
      expect(cv.toMap(), {'test': 2});
      cv.field('test').clear();
      expect(cv.fields, []);
      expect(cv.toMap(), {});
    });
    test('fromMap', () async {
      try {
        IntContent().fromMap(null);
        fail('should have failed');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
      var content = IntContent()..fromMap({});
      expect(content.value.hasValue, isFalse);
    });
    test('allToMap', () async {
      var note = Note()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = DateTime(1).toUtc();
      expect(note.toMap(), {
        'title': 'my_title',
        'content': 'my_content',
        'date': DateTime(1).toUtc()
      });
      expect(note.toMap(columns: [note.title.name]), {'title': 'my_title'});
    });
  });
}
