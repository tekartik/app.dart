import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/src/path.dart';
import 'package:test/test.dart';

class SchoolStudentPath extends PathBase {
  final school = Field<String>('school');
  final student = Field<String>('student');

  @override
  List<Field<String>> get fields => [school, student];
}

class SimplePath extends PathBase {
  final section = Field<String>('section');

  @override
  List<Field<String>> get fields => [section];
}

void main() {
  group('Path', () {
    test('fromString', () {
      var path = ContentPath.fromString('/');
      expect(path.fields, []);
      path = ContentPath.fromString('/');
      expect(path.fields, []);
      path = ContentPath.fromString('//');
      expect(path.fields, []);
      path = ContentPath.fromString('/test');
      expect(path.fields, [Field('test')]);
      path = ContentPath.fromString('/test/');
      expect(path.fields, [Field('test')]);
      path = ContentPath.fromString('/test');
      expect(path.fields, [Field('test')]);
      path = ContentPath.fromString('test');
      expect(path.fields, [Field('test')]);
      path = ContentPath.fromString('/test/1');
      expect(path.fields, [Field('test', '1')]);
      path = ContentPath.fromString('/test/1/sub');
      expect(path.fields, [Field('test', '1'), Field('sub')]);
      path = ContentPath.fromString('test/1/sub/2');
      expect(path.fields, [Field('test', '1'), Field('sub', '2')]);

      // set status
      path = ContentPath.fromString('test');
      expect(path.fields.first.v, null);
      expect(path.fields.first.hasValue, true);
    });
    test('toPath', () {
      String rt(String path) {
        return ContentPath.fromString(path).toPath();
      }

      expect(rt('/'), '/');
      expect(rt('/test/'), 'test');
      expect(rt('test/1/sub/2'), 'test/1/sub/2');

      var object = SchoolStudentPath()
        ..student.v = '124'
        ..school.v = 'the_school';
      expect(object.toPath(), 'school/the_school/student/124');

      object = SchoolStudentPath();
      expect(object.toPath(), 'school/*/student/*');
    });

    test('Path.toPath', () {
      var simple = SimplePath();
      expect(simple.toPath(), 'section/*');
      simple.section.setNull();
      expect(simple.toPath(), 'section');
      simple.section.clear();
      expect(simple.toPath(), 'section/*');
      simple.section.v = '123';
      expect(simple.toPath(), 'section/123');

      var object = SchoolStudentPath();
      expect(object.toPath(), 'school/*/student/*');
      object.student.setNull();
      expect(object.toPath(), 'school/*/student');
    });

    test('toMap', () {
      Map<String, String> rt(String path) {
        return ContentPath.fromString(path).toMap()?.cast<String, String>();
      }

      expect(rt('/'), {});
      // expect(rt('/test/'), {'test': null});
      expect(rt('/test/'), {'test': null}); // !!!
      expect(rt('/test/1'), {'test': '1'});
      expect(rt('test/1/sub/2'), {'test': '1', 'sub': '2'});
    });

    test('matches', () {
      var simple1 = SimplePath();
      var simple2 = SimplePath();
      expect(simple1.matchesPath(simple2), isTrue);
      simple2.section.setNull();
      expect(simple1.matchesPath(simple2), isFalse);
      simple2.section.v = 'any';
      expect(simple1.matchesPath(simple2), isTrue);

      expect(
          ContentPath.fromString('test/1/sub/2')
              .matchesPath(ContentPath.fromString('test/34/sub/28')),
          isTrue);
      expect(
          ContentPath.fromString('test/1/sub/2')
              .matchesPath(ContentPath.fromString('test/34/dub/28')),
          isFalse);
      expect(
          ContentPath.fromString('test/1')
              .matchesPath(ContentPath.fromString('test')),
          isFalse);
    });
    test('fromPath', () {
      var object = SchoolStudentPath()
        ..fromPath(ContentPath.fromString('school/the_school/student/124'));
      expect(object.toString(), 'school/the_school/student/124');
      object = SchoolStudentPath()
        ..fromPath(ContentPath.fromString('school/the_school/student'));
      expect(object.toString(), 'school/the_school/student');
    });
  });
}
