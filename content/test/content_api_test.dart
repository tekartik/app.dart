import 'package:tekartik_app_content/content.dart';
import 'package:test/test.dart';
// ignore_for_file: unnecessary_statements

void main() {
  group('content_api_test', () {
    test('exports', () {
      [
        Column,
        ColumnMixin,
        ColumnNameMixin,
        Content,
        ContentMixin,
        ContentBase,
        ContentValues,
        Field,
        intField,
        stringField
      ];
    });
  });
}
