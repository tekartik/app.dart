import 'package:test/test.dart';
import 'package:tekartik_app_content/content.dart';

void main() {
  group('Column', () {
    test('equals', () async {
      expect(Column('name'), Column('name'));
      expect(Column('name'), isNot(Column('name2')));
      try {
        Column(null);
        fail('should fail');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
    });
  });
}
