import 'package:tekartik_app_content/content.dart';

import 'package:test/test.dart';

import 'content_value_test.dart';

class IntContent extends ContentBase {
  final value = intField('value');

  @override
  List<Field> get fields => [value];
}

void main() {
  group('Content', () {
    test('equals', () {
      expect(IntContent(), IntContent());
      expect(IntContent()..value.v = 1, IntContent()..value.v = 1);
      expect(IntContent(), isNot(IntContent()..value.v = 1));
      expect(IntContent()..value.v = 2, isNot(IntContent()..value.v = 1));
    });
    test('toMap', () async {
      expect(IntContent().toMap(), {});
      expect((IntContent()..value.v = 1).toMap(), {'value': 1});
      expect((IntContent()..value.v = 1).toMap(columns: <String>[]), {});
      expect((IntContent()..value.v = 1).toMap(columns: [IntContent().value.k]),
          {'value': 1});
    });
    test('fromMap', () async {
      try {
        IntContent().fromMap(null);
        fail('should have failed');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
      expect(IntContent()..fromMap({}), IntContent());
      expect(IntContent()..fromMap({'value': 1}), IntContent()..value.v = 1);
      expect(
          IntContent()
            ..fromMap({'value': 1}, columns: [IntContent().value.name]),
          IntContent()..value.v = 1);
      expect(IntContent()..fromMap({'value': 1}, columns: <String>[]),
          IntContent());
    });
    test('allToMap', () async {
      var note = Note()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = DateTime(2020);
      expect(note.toMap(), {
        'title': 'my_title',
        'content': 'my_content',
        'date': DateTime(2020)
      });
      expect(note.toMap(columns: [note.title.name]), {'title': 'my_title'});
    });
  });
}
