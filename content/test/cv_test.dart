import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:test/test.dart';

class Note extends CvModelBase {
  final title = CvField<String>('title');
  final content = CvField<String>('content');
  final date = CvField<int>('date');

  @override
  List<CvField> get cvFields => [title, content, date];
}

class IntContent extends CvModelBase {
  final value = CvField<int>('value');

  @override
  List<CvField> get cvFields => [value];
}

void main() {
  group('cv', () {
    group('Field', () {
      test('cvValuesAreEquals', () {
        expect(cvValuesAreEqual(null, false), isFalse);
        expect(cvValuesAreEqual(null, null), isTrue);
        expect(cvValuesAreEqual(1, 1), isTrue);
        expect(cvValuesAreEqual(1, 2), isFalse);
        expect(cvValuesAreEqual(1, 'text'), isFalse);
        expect(cvValuesAreEqual([1], [1]), isTrue);
        expect(cvValuesAreEqual([1], [2]), isFalse);
        expect(cvValuesAreEqual({'a': 'b'}, {'a': 'b'}), isTrue);
      });
      test('equals', () {
        expect(CvField('name'), CvField('name'));
        expect(CvField('name'), CvField('name', null));
        expect(CvField('name'), isNot(CvField.withValue('name', null)));
        expect(CvField('name'), isNot(CvField.withNull('name')));
        expect(CvField('name', 1), CvField('name', 1));
        expect(CvField('name', [1]), CvField('name', [1]));
        expect(CvField('name', {'a': 'b'}), CvField('name', {'a': 'b'}));
        expect(CvField('name', 1), isNot(CvField('name', 2)));
        expect(CvField('name'), isNot(CvField('name2')));
        expect(CvField('name'), isNot(CvField('name', 1)));
        try {
          CvField(null);
          fail('should fail');
        } catch (e) {
          expect(e, isNot(const TypeMatcher<TestFailure>()));
        }
      });

      test('fromCvField', () {
        expect(CvField<String>('name')..fromField(CvField('name', 'value')),
            CvField('name', 'value'));
      });

      test('hasValue', () {
        var field = CvField('name');
        expect(field.hasValue, isFalse);
        expect(field.v, isNull);
        field.setNull();
        expect(field.hasValue, isTrue);
        expect(field.v, isNull);
        field.clear();
        expect(field.hasValue, isFalse);
        expect(field.v, isNull);
      });
    });
    test('CvModel', () {
      var model = CvMapModel();
      model['test'] = 1;
    });
    test('equals', () {
      expect(IntContent(), IntContent());
      expect(IntContent()..value.v = 1, IntContent()..value.v = 1);
      expect(IntContent(), isNot(IntContent()..value.v = 1));
      expect(IntContent()..value.v = 2, isNot(IntContent()..value.v = 1));
    });
    test('toMap', () async {
      expect(IntContent().toMap(), {});
      expect((IntContent()..value.v = 1).toMap(), {'value': 1});
      expect((IntContent()..value.v = 1).toMap(columns: <String>[]), {});
      expect((IntContent()..value.v = 1).toMap(columns: [IntContent().value.k]),
          {'value': 1});
    });
    test('toModel', () async {
      expect(IntContent().toModel(), {});
      expect((IntContent()..value.v = 1).toMap(), {'value': 1});
      expect((IntContent()..value.v = null).toMap(), {'value': null});
      expect((IntContent()..value.setNull()).toMap(), {'value': null});
      expect((IntContent()..value.setValue(null)).toMap(), {});
      expect((IntContent()..value.setValue(null, presentIfNull: true)).toMap(),
          {'value': null});
    });
    test('fromModel', () async {
      var content = IntContent()..fromModel({});
      expect(content.value.hasValue, false);
      expect(content.value.v, null);
      content = IntContent()..fromModel({'value': null});
      expect(content.value.hasValue, true);
      expect(content.value.v, null);
    });
    test('fromMap', () async {
      try {
        IntContent().fromMap(null);
        fail('should have failed');
      } catch (e) {
        expect(e, isNot(const TypeMatcher<TestFailure>()));
      }
      expect(IntContent()..fromMap({}), IntContent());
      expect(IntContent()..fromMap({'value': 1}), IntContent()..value.v = 1);
      expect(
          IntContent()
            ..fromMap({'value': 1}, columns: [IntContent().value.name]),
          IntContent()..value.v = 1);
      expect(IntContent()..fromMap({'value': 1}, columns: []), IntContent());
    });
    test('allToMap', () async {
      var note = Note()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1;
      expect(note.toMap(),
          {'title': 'my_title', 'content': 'my_content', 'date': 1});
      expect(note.toMap(columns: [note.title.name]), {'title': 'my_title'});
    });
    test('duplicated field', () {
      try {
        WithDuplicatedFields().toMap();
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
      try {
        WithDuplicatedFields().fromMap({});
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
      try {
        WithDuplicatedFields().copyFrom(CvMapModel());
        fail('should fail');
      } on UnsupportedError catch (e) {
        print(e);
      }
    });
    test('content child', () {
      var parent = WithChildField()
        ..child.v = (ChildContent()..sub.v = 'sub_value');
      var map = {
        'child': {'sub': 'sub_value'}
      };
      expect(parent.toMap(), map);
      parent = WithChildField()..fromMap(map);
      expect(parent.toMap(), map);
    });
    test('content child list', () {
      var parent = WithChildListField()
        ..children.v = [ChildContent()..sub.v = 'sub_value'];
      var map = {
        'children': [
          {'sub': 'sub_value'}
        ]
      };
      expect(parent.children.v.first.sub.v, 'sub_value');
      expect(parent.toMap(), map);
      parent = WithChildListField()..fromMap(map);
      expect(parent.toMap(), map);
    });
    test('all types', () {
      AllTypes allTypes;
      void _check() {
        var export = allTypes.toModel();
        var import = AllTypes()..fromModel(export);
        expect(import, allTypes);
        expect(import.toModel(), allTypes.toModel());
        import = AllTypes()..fromModel(jsonDecode(jsonEncode(export)) as Map);

        expect(import.toModel(), allTypes.toModel());
      }

      allTypes = AllTypes();
      _check();
      allTypes
        ..intField.v = 1
        ..numField.v = 2.5
        ..stringField.v = 'some_test'
        ..intListField.v = [2, 3, 4]
        ..mapField.v = {'sub': 'map'}
        ..mapListField.v = [
          {'sub': 'map'}
        ]
        ..children.v = [
          WithChildField()..child.v = (ChildContent()..sub.v = 'sub_value')
        ];
      _check();
    });
  });
}

class WithDuplicatedFields extends CvModelBase {
  final field1 = CvField<String>('field1');
  final field2 = CvField<String>('field1');

  @override
  List<CvField> get cvFields => [field1, field2];
}

class WithChildField extends CvModelBase {
  final child = CvModelField<ChildContent>('child', (_) => ChildContent());

  @override
  List<CvField> get cvFields => [child];
}

class WithChildListField extends CvModelBase {
  final children =
      CvModelListField<ChildContent>('children', (_) => ChildContent());

  @override
  List<CvField> get cvFields => [children];
}

class ChildContent extends CvModelBase {
  final sub = CvField<String>('sub');

  @override
  List<CvField> get cvFields => [sub];
}

class AllTypes extends CvModelBase {
  final intField = CvField<int>('int');
  final numField = CvField<num>('num');
  final stringField = CvField<String>('string');
  final intListField = CvListField<int>('intList');
  final mapField = CvField<Map>('map');
  final mapListField = CvListField<Map>('mapList');
  final children =
      CvModelListField<WithChildField>('children', (_) => WithChildField());

  @override
  List<CvField> get cvFields => [
        intField,
        numField,
        stringField,
        children,
        intListField,
        mapField,
        mapListField
      ];
}
