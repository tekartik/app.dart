import 'package:tekartik_common_utils/model/model.dart';
export 'package:tekartik_common_utils/model/model.dart';
export 'src/cv_field.dart'
    show CvField, CvModelField, CvModelListField, CvListField, cvValuesAreEqual;
export 'src/cv_model.dart'
    show
        CvModel,
        CvMapModel,
        CvModelCore,
        CvModelBase,
        CvModelRead,
        CvModelListWrite,
        CvModelListRead,
        CvModelWrite,
        CvModelBaseMixin;

class CvContent {}

class CvContentList {}

abstract class CvMap {
  Model toModel();
  void fromModel(Map map);
}

abstract class CvList {
  ModelList toModelList();
  void fromModelList(List list);
}
