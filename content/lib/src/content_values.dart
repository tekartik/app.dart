import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_content/src/column.dart';
import 'package:tekartik_app_content/src/cv_model.dart';
import 'package:tekartik_app_content/src/field.dart';
import 'package:tekartik_app_content/src/import.dart';

abstract class CvBase
    with
        CvModelBaseMixin,
        // Order is important, first one wins
        ContentValuesMapMixin,
        ConventValuesKeysFromFieldsMixin,
        ContentMixin,
        MapMixin<String, dynamic> {}

abstract class ContentValues
    implements Content, Map<String, dynamic>, CvMapModel {
  /// Map based content values
  factory ContentValues() => ContentValuesMap();
  factory ContentValues.withFields(List<Field> fields) {
    return _ContentValuesWithFields(fields);
  }
}

/// Field in the map base implementation
class _CvMapField<T>
    with FieldMixin<T>, ColumnMixin<T>, ColumnNameMixin
    implements Field<T> {
  final ContentValues cv;

  /// Only set value if not null
  _CvMapField(this.cv, String name, [T value]) {
    this.name = name;
    setValue(value);
  }

  /// Force a null value
  _CvMapField.withNull(this.cv, String name) {
    this.name = name;
    setNull();
  }

  @override
  T get v => cv[name] as T;

  @override
  Field<RT> cast<RT>() =>
      T == RT ? this as Field<RT> : _CvMapField<RT>(cv, name);

  @override
  void clear() => cv.remove(name);

  @override
  void fromField(CvField field) {
    // copy the value whatever the name is
    if (field.hasValue) {
      cv[name] = field.v;
    } else {
      cv.remove(name);
    }
  }

  @override
  bool get hasValue => cv.containsKey(name);

  @override
  bool get isNull => cv[name] == null;

  @override
  String get k => name;

  @override
  void removeValue() {
    cv.remove(name);
  }

  @override
  void setNull() {
    cv[name] = null;
  }

  @override
  void setValue(value, {bool presentIfNull = false}) {
    if (value != null) {
      cv[name] = value;
    } else if (presentIfNull) {
      cv[name] = null;
    } else {
      cv.remove(name);
    }
  }

  @override
  set v(T value) {
    cv[name] = value;
  }
}

mixin _MapBaseMixin implements Map<String, dynamic> {
  Map<String, dynamic> _map;

  @override
  dynamic operator [](Object key) => _map[key];

  @override
  void operator []=(String key, value) => _map[key] = value;

  @override
  void clear() => _map.clear();

  @override
  Iterable<String> get keys => _map.keys;

  @override
  dynamic remove(Object key) => _map.remove(key);
}

// ignore: unused_element
class _TestClass with _MapBaseMixin, MapMixin<String, dynamic> {}

/// A Map based implementation. Default implementation for content values
class ContentValuesMap
    with
// Order is important, first one wins
        CvModelBaseMixin,
        ContentMixin,
        _MapBaseMixin,
        MapMixin<String, dynamic>
    //ContentValuesMapMixin
    implements
        ContentValues {
  ContentValuesMap([Map<String, dynamic> map]) {
    _map = map ?? <String, dynamic>{};
  }
  @override
  List<Field> get fields => keys
      .map((name) => field<dynamic>(name))
      .where((field) => field != null)
      .toList();

  @override
  Field<T> field<T>(String name) {
    var value = this[name];
    if (value != null) {
      return _CvMapField(this, name, value as T);
    } else {
      if (containsKey(name)) {
        return _CvMapField<T>.withNull(this, name);
      }
    }
    return null;
  }

  @override
  void fromMap(Map map, {List<String> columns}) {
    if (columns == null) {
      map.forEach((key, value) {
        _map[key.toString()] = value;
      });
    } else {
      for (var column in columns) {
        if (map.containsKey(column)) {
          _map[column] = map[column];
        }
      }
    }
  }

  /*
  @override
  Model toMap({List<String> columns}) => columns == null
      ? asModel(_map)
      : (Model(_map)..removeWhere((key, value) => !columns.contains(key)));

   */
}

/// Keys from fields
mixin ConventValuesKeysFromFieldsMixin implements ContentValues {
  @override
  Iterable<String> get keys => fields.map((field) => field.name);
}

mixin ContentValuesMapMixin implements ContentValues {
  @override
  dynamic operator [](Object key) {
    return field(key?.toString())?.v;
  }

  @override
  void operator []=(key, value) {
    field(key?.toString())?.v = value;
  }

  @override
  void clear() {
    fields.forEach((field) => field.clear());
  }

  @override
  dynamic remove(Object key) {
    field(key?.toString())?.clear();
  }
}

class _ContentValuesWithFields extends CvBase {
  @override
  final List<Field> fields;

  _ContentValuesWithFields(this.fields);
}
