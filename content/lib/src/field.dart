import 'package:meta/meta.dart';
import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/cv.dart';

import 'column.dart';
import 'cv_field.dart';

/// Field content
abstract class Field<T> implements CvFieldCore<T>, CvField<T> {
  /// Only set value if not null
  factory Field(String name, [T value]) => FieldImpl(name, value);

  /// Force a null value
  factory Field.withNull(String name) => FieldImpl.withNull(name);

  /// Force a value event if null
  factory Field.withValue(String name, T value) =>
      FieldImpl.withValue(name, value);
}

/// Field access
///
/// Use [v] for value access.
abstract class CvFieldCore<T> implements Column<T> {
  /// The value
  T get v;

  /// The key
  String get k;

  /// Return true is null of unset
  bool get isNull;

  /// Set the value, even if null
  set v(T value);

  /// Clear value and flag
  void clear();

  // to be deprecated use clear instead
  @deprecated
  void removeValue();

  /// [presentIfNull] true if null is marked as a value
  void setValue(T value, {bool presentIfNull = false});

  bool get hasValue;

  /// Allow dynamic fields
  @visibleForTesting
  void fromField(CvField field);

  /// Cast if needed
  Field<RT> cast<RT>();

  /// Force the null value.
  void setNull();
}

/// Nested field content
abstract class FieldContent<T extends CvModel> implements Field<T> {
  /// contentValue should be ignored
  T create(dynamic contentValue);

  /// Only set value if not null
  factory FieldContent(String name, T Function(dynamic contentValue) create) =>
      FieldContentImpl(name, create);
}

/// Nested list
abstract class FieldContentList<T extends CvModel> implements Field<List<T>> {
  /// contentValue should be ignored or could be used to create the proper object
  /// but its content should not be populated.
  T create(dynamic contentValue);
  List<T> createList();

  /// Only set value if not null
  factory FieldContentList(
          String name, T Function(dynamic contentValue) create) =>
      FieldContentListImpl(name, create);
}

/// Nested list implementation.
class ListFieldImpl<T> extends FieldImpl<List<T>>
    implements CvField<List<T>>, CvListField<T> {
  @override
  List<T> createList() => <T>[];

  ListFieldImpl(String name) : super(name);
}

/// Nested list of object implementation.
class FieldContentListImpl<T extends CvModel> extends FieldImpl<List<T>>
    implements FieldContentList<T>, CvModelListField<T> {
  @override
  List<T> createList() => <T>[];
  final T Function(dynamic contentValue) _create;
  FieldContentListImpl(String name, this._create) : super(name);

  @override
  T create(contentValue) => _create(contentValue);
}

class FieldContentImpl<T extends CvModel> extends FieldImpl<T>
    implements FieldContent<T>, CvModelField<T> {
  final T Function(dynamic contentValue) _create;
  FieldContentImpl(String name, this._create) : super(name);

  @override
  T create(contentValue) => _create(contentValue);
}

class FieldImpl<T>
    with // order is important, 2020/11/08 last one wins!
        ColumnMixin<T>,
        ColumnNameMixin,
        FieldMixin<T> {
  /// Only set value if not null
  FieldImpl(String name, [T value]) {
    assert(name != null, 'field name cannot be null');
    this.name = name;
    if (value != null) {
      v = value;
    }
  }

  /// Force a null value
  FieldImpl.withNull(String name) {
    this.name = name;
    _hasValue = true;
  }

  /// Set value even if null
  FieldImpl.withValue(String name, T value) {
    assert(name != null, 'field name cannot be null');
    this.name = name;
    v = value;
  }
}

// ensure mixin compiles
// ignore: unused_element
class _TestField
    with ColumnNameMixin, ColumnMixin, FieldMixin
    implements Field {}

mixin FieldMixin<T> implements Field<T> {
  T _value;

  /// The value
  @override
  T get v => _value;

  /// The key
  @override
  String get k => name;

  @override
  bool get isNull => _value == null;

  @override
  set v(T value) {
    _hasValue = true;
    _value = value;
  }

  /// Clear value and flag
  @override
  void clear() {
    _value = null;
    _hasValue = false;
  }

  // to be deprecated use clear instead
  @override
  @deprecated
  void removeValue() {
    _value = null;
    _hasValue = false;
  }

  /// [presentIfNull] true if null is marked as a value
  @override
  void setValue(T value, {bool presentIfNull = false}) {
    if (value == null) {
      if (presentIfNull) {
        v = value;
      } else {
        clear();
      }
    } else {
      v = value;
    }
  }

  bool _hasValue = false;

  @override
  bool /*!*/ get hasValue => _hasValue;

  /// Allow dynamic fields
  @override
  @visibleForTesting
  void fromField(CvField field) {
    setValue(field.v as T, presentIfNull: field.hasValue);
  }

  @override
  String toString() => '$name: $v${(v == null && hasValue) ? ' (set)' : ''}';

  /// Cast if needed
  @override
  Field<RT> cast<RT>() {
    if (this is Field<RT>) {
      return this as Field<RT>;
    }
    return Field<RT>(name)..v = v as RT;
  }

  @override
  int get hashCode => super.hashCode + (v?.hashCode ?? 0);

  @override
  bool operator ==(other) {
    if (other is CvField) {
      if (other.name != name) {
        return false;
      }
      if (other.hasValue != hasValue) {
        return false;
      }
      if (!cvValuesAreEqual(other.v, v)) {
        return false;
      }
      return true;
    }
    return false;
  }

  /// Force the null value.
  @override
  void setNull() {
    setValue(null, presentIfNull: true);
  }
}

Field<int> intField(String name) => Field<int>(name);

Field<String> stringField(String name) => Field<String>(name);

/// List<Column> helpers
extension CvColumnExtension on List<Column> {
  List<String> get names => map((c) => c.name).toList();
}
