String logTruncate(String text) {
  var len = 128;
  if (text != null && text.length > len) {
    text = text.substring(0, len);
  }
  return text;
}

/// True for null, num, String, bool
bool isBasicTypeOrNull(dynamic value) {
  if (value == null) {
    return true;
  } else if (value is num || value is String || value is bool) {
    return true;
  }
  return false;
}
