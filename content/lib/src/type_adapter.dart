import 'dart:convert';

class TypeConverter<S, T> extends Converter<S, T> {
  final T Function(S input) _convert;

  TypeConverter(this._convert);

  @override
  T convert(S input) => _convert(input);
}

/// Base type adapter codec
abstract class TypeAdapter<S, T> extends Codec<S, T> {
  /// name used in the annoation '@${name}'
  String get name;

  /// True if the value is the proper type.
  bool isType(dynamic value);
}

/// Mixin for type adapters
mixin TypeAdapterCodecMixin<S, T> implements TypeAdapter<S, T> {
  // bool get isType(dynamic value);

  @override
  bool isType(dynamic value) => value is S;

  @override
  Converter<S, T> encoder;
  @override
  Converter<T, S> decoder;

  @override
  String toString() => 'TypeAdapter($name)';
}
