import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/cv.dart';

abstract class CvModelRead {
  Model toModel();
}

abstract class CvModelWrite {
  void fromModel(Map map);
}

abstract class CvModelCore {}

/// Modifiable map.
abstract class CvMapModel implements CvModel, Map<String, dynamic>, Content {
  /// Basic content values factory
  factory CvMapModel() => ContentValues();
}

/// Model to access the data
abstract class CvModel implements CvModelRead, CvModelWrite, CvModelCore {
  CvField<T> cvField<T>(String name);
}

abstract class CvModelListRead {
  ModelList toModelList();
}

abstract class CvModelListWrite {
  void fromModelList(List map);
}

/// Base content class
abstract class CvModelBase with CvModelBaseMixin, ContentMixin {
  List<CvField> get cvFields;

  @override
  List<Field> get fields => cvFields?.cast<Field>();
}

/// Base model evolution
mixin CvModelBaseMixin implements CvModel, Content {
  @override
  void fromModel(Map map) {
    fromMap(map);
  }

  @override
  Model toModel() => toMap();

  @override
  CvField<T> cvField<T>(String name) {
    // TODO: implement cvField
    throw UnimplementedError();
  }
}

// ignore: unused_element
class _CvModelMock extends CvModelBase {
  @override
  List<CvField> get cvFields => null;
}
