import 'package:path/path.dart' as p;
import 'package:tekartik_app_content/src/content_values.dart';
import 'package:tekartik_app_content/src/cv_model.dart';

import 'import.dart';

/// Base path class, just declare your fields.
abstract class PathBase
    with CvModelBaseMixin, ContentMixin, PathMixin
    implements ContentPath {
  @override
  String toString() => toPath();
}

/// Like firestore type/id/type/id in url format for url reference
abstract class ContentPath implements Content {
  static var separator = p.url.separator;

  /// Field accessor
  @override
  List<Field<String>> get fields;

  /// From a coll1/id1/coll2/id2
  factory ContentPath.fromString(String path) {
    return _PathFromPath(path);
  }

  void fromPath(ContentPath path);

  String toPath();

  /// A map representing the data, but likely unorderd.
  Map<String, String> toStringMap();

  /// If both path matches /one/1/two/2 matching /one/34/two/27
  bool matchesPath(ContentPath path);

  @override
  String toString() => toPath();
}

mixin PathMixin implements ContentPath, Content {
  @override
  String toPath() {
    var sb = StringBuffer();
    for (var field in fields) {
      if (sb.isNotEmpty) {
        sb.write(ContentPath.separator);
      }
      var name = field.name;
      var value = field.v;
      sb.write(name);
      if (value != null) {
        sb.write(ContentPath.separator);
        sb.write(value);
      } else if (!field.hasValue) {
        sb.write(ContentPath.separator);
        sb.write('*');
      }
    }
    if (sb.isEmpty) {
      return ContentPath.separator;
    }
    return sb.toString();
  }

  @override
  void fromPath(ContentPath path) {
    for (var field in fields) {
      field.v = path.field(field.name)?.v?.toString();
    }
  }

  @override
  Map<String, String> toStringMap() {
    return ContentValues.withFields(fields)?.cast<String, String>();
  }

  /// If both path matches /one/1/two/2 matching /one/34/two/27
  @override
  bool matchesPath(ContentPath path) {
    if (fields.length == path.fields.length) {
      for (var i = 0; i < fields.length; i++) {
        var f1 = fields[i];
        var f2 = path.fields[i];
        if (f1.name != f2.name) {
          return false;
        }
        // Ok
        // not set and non null value ok

        // Either both are non or not  set, either none
        var v1Set = f1.hasValue;
        var v2Set = f2.hasValue;
        var v1 = f1.v;
        var v2 = f2.v;

        var isNull1 = v1Set && v1 == null;
        var isNull2 = v2Set && v2 == null;

        if (isNull1) {
          return isNull2;
        } else {
          if (isNull2) {
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }
}

class _PathFromPath extends PathBase {
  @override
  final fields = <Field<String>>[];

  _PathFromPath(String path) {
    var parts = p.url.split(path);
    // devPrint(parts);
    var start = 0;
    if (parts[0] == ContentPath.separator) {
      start++;
    }
    var end = parts.length;
    if (parts[end - 1] == ContentPath.separator) {
      end--;
    }
    for (var i = start; i < end; i++) {
      var name = parts[i++];
      String /*?*/ value;
      var field = Field<String>(name);
      if (i < end) {
        value = parts[i];
        // Won't set if null however cannot happen
        field.setValue(value);
      } else {
        // set null explicitely
        field.setNull();
      }

      fields.add(field);
    }
  }
  @override
  String toString() => toPath();
}
