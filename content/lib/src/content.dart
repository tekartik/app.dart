import 'package:tekartik_app_content/content.dart';
import 'package:tekartik_app_content/cv.dart';
import 'package:tekartik_app_content/src/cv_field.dart';
import 'package:tekartik_app_content/src/cv_model.dart';
import 'package:tekartik_app_content/src/import.dart';
import 'package:tekartik_common_utils/env_utils.dart';
import 'package:tekartik_common_utils/model/model.dart';

import 'field.dart';
import 'utils.dart';

var debugContent = false; // devWarning(true);

abstract class Content implements CvModel {
  /// to override something like [name, description]
  List<Field> get fields;

  Field<T> field<T>(String name);

  /// If 2 content are equals
  static bool equals(Content content1, Content content2) {
    if (content1.fields.length != content2.fields.length) {
      return false;
    }
    for (var field in content1.fields) {
      if (content2.field(field.name) != field) {
        return false;
      }
    }
    return true;
  }

  /// Convert to map
  Model toMap({List<String> columns});

  /// Read from map
  void fromMap(Map map, {List<String> columns});
}

/// Base content class
abstract class ContentBase with ContentMixin, CvModelBaseMixin {}

/// Content mixin
mixin ContentMixin implements Content {
  @override
  void fromMap(Map map, {List<String> columns}) {
    _debugCheckFields();
    assert(map != null, 'map cannot be null');
    columns ??= fields.map((e) => e.name).toList();
    var model = Model(map);
    for (var column in columns) {
      try {
        var field = this.field(column);
        var entry = model.getModelEntry(field.name);
        if (entry != null) {
          if (field is FieldContentList) {
            var list = field.v = field.createList();
            for (var rawItem in entry.value as List) {
              var item = field.create(rawItem)..fromModel(rawItem as Map);
              list.add(item);
            }
            field.v = list;
          } else if (field is FieldContent) {
            field.v = field.create(entry.value)..fromModel(entry.value as Map);
          } else if (field is CvListField) {
            var list = field.v = (field as CvListField).createList();
            for (var rawItem in entry.value as List) {
              list.add(rawItem);
            }
            field.v = list;
          } else {
            field.v = entry.value;
          }
        }
      } catch (e) {
        if (debugContent) {
          print('ERROR fromMap($map, $columns) at $column');
        }
      }
    }
  }

  /// Copy content
  void copyFrom(CvModel model) {
    _debugCheckFields();
    for (var field in fields) {
      var recordField = model.cvField(field.name);
      if (recordField?.hasValue == true) {
        // ignore: invalid_use_of_visible_for_testing_member
        field.fromField(recordField);
      }
    }
  }

  /// Copy content
  void fromContent(Content snapshot) {
    _debugCheckFields();
    for (var field in fields) {
      var recordField = snapshot.field(field.name);
      if (recordField?.hasValue == true) {
        // ignore: invalid_use_of_visible_for_testing_member
        field.fromField(recordField);
      }
    }
  }

  void _debugCheckFields() {
    if (isDebug) {
      var success = _debugFieldsCheckDone[runtimeType];

      if (success == null) {
        var _fieldNames = <String>{};
        for (var field in fields) {
          if (_fieldNames.contains(field.name)) {
            _debugFieldsCheckDone[runtimeType] = false;
            throw UnsupportedError(
                'Duplicated field ${field.name} in $runtimeType${fields.map((f) => f.name)} - $this');
          }
          _fieldNames.add(field.name);
        }
        _debugFieldsCheckDone[runtimeType] = success = true;
      } else if (!success) {
        throw UnsupportedError(
            'Duplicated fields in $runtimeType${fields.map((f) => f.name)} - $this');
      }
    }
  }

  @override
  Model toMap({List<String> columns}) {
    _debugCheckFields();
    columns ??= fields.map((e) => e.name).toList();
    var model = Model();
    for (var column in columns) {
      var field = this.field(column);
      dynamic value = field.v;
      if (value is List<Content>) {
        value = (value as List).map((e) => (e as Content).toMap()).toList();
      }
      if (value is Content) {
        value = value.toMap();
      }
      model.setValue(field.name, value, presentIfNull: field.hasValue);
    }
    return model;
  }

  @override
  String toString() {
    try {
      return logTruncate('${toMap()}');
    } catch (e) {
      return logTruncate('${fields} $e');
    }
  }

  // Only created if necessary
  Map<String, Field> _fieldMap;

  @override
  Field<T> field<T>(String name) {
    _fieldMap ??=
        Map.fromEntries(fields.map((field) => MapEntry(field.name, field)));
    return _fieldMap[name]?.cast<T>();
  }

  @override
  int get hashCode => fields.first.hashCode;

  @override
  bool operator ==(other) {
    if (other is Content) {
      return Content.equals(this, other);
    }
    return false;
  }
}

final _debugFieldsCheckDone = <Type, bool>{};
