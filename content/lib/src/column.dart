abstract class RawColumn {
  String get name;
}

abstract class Column<T> implements RawColumn {
  factory Column(String name) => ColumnImpl(name);

  Type get type;

  bool get isTypeInt;

  bool get isTypeString;
}

class ColumnImpl<T> with ColumnMixin<T>, ColumnNameMixin implements Column<T> {
  ColumnImpl(String name) {
    assert(name != null, 'column name cannot be null');
    this.name = name;
  }
}

mixin ColumnNameMixin implements RawColumn {
  @override
  String name;

  @override
  bool operator ==(other) {
    if (other is RawColumn) {
      return other.name == name;
    }
    return false;
  }
}

mixin ColumnMixin<T> implements Column<T> {
  @override
  Type get type => T;

  @override
  int get hashCode => name.hashCode;

  @override
  bool get isTypeInt => T == int;

  @override
  bool get isTypeString => T == String;

  @override
  String toString() => 'Column($name)';
}
