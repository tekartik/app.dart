## Setup

```yaml
dependencies:
  tekartik_app_content:
    git:
      url: https://gitlab.com/tekartik/app.dart
      path: content
    version: '>=0.1.0'
```