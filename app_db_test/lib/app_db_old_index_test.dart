import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

import 'app_db_test.dart';

class DbNote extends DbContent {
  final title = stringField('title');
  final date = intField('date');

  @override
  List<Field> get fields => [title, date];
}

DbNote buildNote() => DbNote();

void main() {
  var provider = inMemoryDatabaseProvider;
  providerTest(provider);
}

void providerTest(DatabaseProvider provider) {
  group('Index', () {
    Future<String> preparedDbName() async {
      var name = 'index_test.db';
      await provider.deleteDatabase(name);
      return name;
    }

    test('add/get', () async {
      var store = StoreRef('note');
      var model = DbNote();
      var index = store.index(column: model.date);

      var note = DbNote()
        ..title.v = 'my_title'
        ..date.v = 1234;
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                var openTransactionStore = txn.addStoreOld(store, DbNote());
                openTransactionStore.addIndex(index);
              }));
      var key = await store.addOld(db, note);
      expect(key, 1);

      var snapshot = await index.query().first(db, 1234);
      expect(snapshot.fields.contains(Field('title', 'my_title')), isTrue);
      expect(snapshot.field<String>('title').v, 'my_title');

      var note2 = DbNote()
        ..title.v = 'other_title'
        ..date.v = 5678;
      await store.addOld(db, note2);

      var snapshots = await index
          .query(start: Boundary(value: 5678, include: true))
          .get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'other_title', 'date': 5678, '_id': 2}
      ]);

      var note3 = DbNote()
        ..title.v = 'yet_another_title'
        ..date.v = 5678;
      await store.addOld(db, note3);
      var note4 = DbNote()
        ..title.v = 'title_4'
        ..date.v = 5678;
      await store.addOld(db, note4);
      var query = index.query(
          start: Boundary(value: 5678, include: true),
          finder: Finder(limit: 1));
      snapshots = await query.get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'other_title', 'date': 5678, '_id': 2}
      ]);
      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'yet_another_title', 'date': 5678, '_id': 3},
      ]);

      query = index.query(
          start: Boundary(value: 5678, include: true),
          finder: Finder(limit: 1, offset: 1));

      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'title_4', 'date': 5678, '_id': 4}
      ]);
      //expect(snapshots.cursor, isNull);
      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMapOld(snapshots), []);

      // end cursor
      query = index.query(
        end: Boundary(value: 5678, include: false),
      );
      snapshots = await query.get(db);

      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'my_title', 'date': 1234, '_id': 1}
      ]);
      query = index.query(
        end: Boundary(value: 1234, include: true),
      );
      snapshots = await query.get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'my_title', 'date': 1234, '_id': 1}
      ]);

      // end and cursor
      query = index.query(end: Boundary(value: 1234, include: true));
      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMapOld(snapshots), []);

      await db.close();
    });

    test('tuple', () async {
      var store = StoreRef('note');
      var model = DbNote();
      var indexColumn =
          TupleColumn2<int, String>.fromColumns(model.date, model.title);
      var index = store.index(column: indexColumn);

      var note1 = DbNote()
        ..title.v = 'title1'
        ..date.v = 1234;
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                print('onVersionChanged');
                var openTransactionStore = txn.addStoreOld(store, DbNote());
                openTransactionStore.addIndex(index);
              }));
      var key = await store.addOld(db, note1);
      expect(key, 1);

      var snapshot =
          await index.query().first(db, indexColumn.values(1234, 'title1'));
      expect(snapshotToDebugMapOld(snapshot),
          {'title': 'title1', 'date': 1234, '_id': 1});

      expect(await index.query().first(db, indexColumn.values(1234, 'title2')),
          isNull);
      expect(await index.query().first(db, indexColumn.values(1233, 'title1')),
          isNull);

      var note2 = DbNote()
        ..title.v = 'title2'
        ..date.v = 1234;
      await store.addOld(db, note2);

      var snapshots = await index
          .query(start: Boundary(value: indexColumn.values(1234, 'title1')))
          .get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'title2', 'date': 1234, '_id': 2}
      ]);

      snapshots = await index
          .query(end: Boundary(value: indexColumn.values(1234, 'title2')))
          .get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'title1', 'date': 1234, '_id': 1}
      ]);

      snapshots = await index
          .query(
              start: Boundary(value: indexColumn.values(1234, 'title1')),
              end: Boundary(value: indexColumn.values(1234, 'title2')))
          .get(db);
      expect(snapshotsToDebugMapOld(snapshots), []);

      snapshots = await index
          .query(
              start: Boundary(
                  value: indexColumn.values(1234, 'title1'), include: true),
              end: Boundary(
                  value: indexColumn.values(1234, 'title2'), include: true))
          .get(db);
      expect(snapshotsToDebugMapOld(snapshots), [
        {'title': 'title1', 'date': 1234, '_id': 1},
        {'title': 'title2', 'date': 1234, '_id': 2}
      ]);
      /*

      var note3 = DbNote()
        ..title.v = 'yet_another_title'
        ..content.v = 'yes_another_content'
        ..date.v = 5678;
      await store.add(db, note3);
      var note4 = DbNote()
        ..title.v = 'title_4'
        ..content.v = 'content_4'
        ..date.v = 5678;
      await store.add(db, note4);
      var query = index.query(
          start: Boundary(value: 5678, include: true),
          finder: Finder(limit: 1));
      snapshots = await query.get(db);
      expect(snapshotsToDebugMap(snapshots), [
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMap(snapshots), [
        {
          'title': 'yet_another_title',
          'content': 'yes_another_content',
          'date': 5678,
          '_id': 3
        },
      ]);

      query = index.query(
          start: Boundary(value: 5678, include: true),
          finder: Finder(limit: 1, offset: 1));

      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'title_4', 'content': 'content_4', 'date': 5678, '_id': 4}
      ]);
      //expect(snapshots.cursor, isNull);
      snapshots = await query.get(db, cursor: snapshots.cursor);
      expect(snapshotsToDebugMap(snapshots), []);
  */
      await db.close();
    });
  });
}
