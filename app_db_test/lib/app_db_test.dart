import 'package:tekartik_app_db/app_db.dart';
// ignore: implementation_imports
import 'package:tekartik_app_db/src/record_snapshot.dart';
// ignore: implementation_imports
import 'package:tekartik_app_db/src/result_set.dart';
import 'package:test/test.dart';

class DbNote extends DbContent {
  final title = stringField('title');
  final content = stringField('content');
  final date = intField('date');

  @override
  List<Field> get fields => [title, content, date];
}

class DbMinContent extends DbContent {
  final text = stringField('text');

  @override
  List<Field> get fields => [text];
}

void main() {
  var provider = inMemoryDatabaseProvider;
  providerTest(provider);
}

void providerTest(DatabaseProvider provider) {
  group('Db', () {
    test('delete', () async {
      var provider = inMemoryDatabaseProvider;
      var db = 'test.db';
      await provider.deleteDatabase(db);
    });
    Future<String> preparedDbName() async {
      var name = 'test.db';
      await provider.deleteDatabase(name);
      return name;
    }

    test('open/close', () async {
      var provider = inMemoryDatabaseProvider;
      var name = 'test.db';
      await provider.deleteDatabase(name);
      var db = await provider.openDatabase(name, options: DatabaseOptions());
      await db.close();
    });

    test('add/get', () async {
      var store = StoreRef('note');

      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStore(store);
              }));
      var key = await store.add(db, {'test': 1});
      expect(key, 1);
      expect(await store.record(key).get(db), {'test': 1});

      await db.close();
    });

    test('add/get transaction', () async {
      var store = StoreRef('note');

      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStore(store);
              }));
      await db.transaction((txn) async {
        var key = await store.add(txn, {'test': 1});
        expect(key, 1);
        expect(await store.record(key).get(txn), {'test': 1});
      }, mode: TransactionMode.readWrite);

      await db.close();
    });

    test('add/get/finder', () async {
      var store = StoreRef('note');
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1234;
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStore(store);
              }));
      var key = await store.add(db, note.toMap());
      expect(key, 1);
      var snapshot = await store.record(key).getSnapshot(db);
      expect(snapshot.value,
          {'title': 'my_title', 'content': 'my_content', 'date': 1234});

      var snapshots = await store.query().get(db);
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1}
      ]);

      // Same in transaction
      await db.transaction((txn) async {
        snapshots = await store.query().get(txn);
      });
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1}
      ]);

      var note2 = DbNote()
        ..title.v = 'other_title'
        ..content.v = 'other_content'
        ..date.v = 5678;
      var key2 = await store.add(db, note2.toMap());

      snapshots = await store.query().get(db);
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      snapshots = await store.query().all(db);
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      expect((await store.record(key).getSnapshot(db)).toDebugMap(), {
        'title': 'my_title',
        'content': 'my_content',
        'date': 1234,
        '_id': 1
      });
      expect((await store.record(key2).getSnapshot(db)).toDebugMap(), {
        'title': 'other_title',
        'content': 'other_content',
        'date': 5678,
        '_id': 2
      });

      snapshots = await store.query(finder: Finder(limit: 1)).get(db);
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
      ]);
      snapshots = await store.query(finder: Finder(offset: 1)).get(db);
      expect(resultSetToDebugMap(snapshots), [
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      var resultSet = await store.query(finder: Finder(limit: 1)).get(db);
      snapshots = await store.query().get(db, cursor: resultSet.cursor);
      expect(resultSetToDebugMap(snapshots), [
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);

      // Wrong access, kept for testing.
      snapshots = await store.query(finder: Finder(limit: 1)).all(db);
      expect(resultSetToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);

      await db.close();
    });

    test('get object', () async {
      var store = StoreRef('test');
      var content = DbMinContent()..text.v = 'text';
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbMinContent());
              }));
      var key = await store.addOld(db, content);
      content =
          await store.record(key).getDbSnapshot(db, content: DbMinContent());
      expect(content.toDebugMap(), {'text': 'text', '_id': 1});

      await db.close();
    });

    test('clear', () async {
      var store = StoreRef('test');
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStore(store);
              }));
      var key = await store.add(db, {'text': 'text'});
      var snapshots = await store.query().get(db);
      expect(resultSetToDebugMap(snapshots), [
        {'text': 'text', '_id': 1}
      ]);
      await store.clear(db);
      expect(await store.record(key).getSnapshot(db), isNull);

      await db.close();
    });

    // New api
    test('add/put/get', () async {
      var store = DbSchema.dbStore<DbNote>('test', builder: () => DbNote());
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStore(store.ref);
              }));
      var dbNote = DbNote()..title.v = 'my_title';
      var record = await store.add(db, dbNote);
      var readDbNote = await record.get(db);
      expect(readDbNote, isNot(dbNote));
      expect(Content.equals(readDbNote, dbNote), isTrue);

      readDbNote.title.v = 'new_title';
      expect(await record.set(db, readDbNote), readDbNote);

      await db.close();
    });
  });
}

List<Map<String, dynamic>> snapshotsToDebugMapOld(
    Iterable<DbSnapshot> snapshots) {
  return snapshots.map((snapshot) => snapshotToDebugMapOld(snapshot)).toList();
}

List<Map<String, dynamic>> resultSetToDebugMap(ResultSet resultSet) {
  return resultSet.toDebugMap();
}

Map<String, dynamic> snapshotToDebugMapOld(DbSnapshot snapshot) {
  return snapshot.toDebugMap();
}

Map<String, dynamic> snapshotToDebugMap(RecordSnapshot snapshot) {
  return snapshot.toDebugMap();
}
