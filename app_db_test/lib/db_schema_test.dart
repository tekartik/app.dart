import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

import 'test_db_data.dart';

void main() {
  var provider = inMemoryDatabaseProvider;
  providerDbSchemaGroup(provider);
}

void providerDbSchemaGroup(DatabaseProvider provider) {
  group('DbSchema', () {
    test('with index', () async {
      var noteStore = DbSchema.dbStore<DbNote>(
        'note',
        builder: () => DbNote(),
      );
      var model = noteStore.model;
      var index = noteStore.index(column: model.date);
      noteStore.indecies.add(index);

      var schema = DbSchema.db(stores: [noteStore]);
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1;
      expect(schema.stores.first.name, 'note');

      var name = 'journal';
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 2,
              schema: schema,
              onVersionChanged: (txn, oldVersion, newVersion) {
                if (oldVersion < 2) {
                  schema.create(txn);
                }
              }));
      var record = await noteStore.add(db, note);
      var readNote = await record.get(db);
      expect(readNote.title, note.title);

      readNote = await index.get(db, readNote.date.v);
      expect(readNote.title, note.title);
      await db.close();
      db = await provider.openDatabase(name,
          options: DatabaseOptions(schema: schema));
      readNote = await record.get(db);
      expect(readNote.title, note.title);

      readNote = await index.get(db, readNote.date.v);
      expect(readNote.title, note.title);
      await db.close();

      // Read meta
      //db = await provider.openDatabase(name);
      //var metaStore = StoreRef('meta');
      //metaStore.
    });
  });
}
