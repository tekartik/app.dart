import 'package:tekartik_app_db/app_db.dart';
import 'package:test/test.dart';

class DbNote extends DbContent {
  final title = stringField('title');
  final content = stringField('content');
  final date = intField('date');

  @override
  List<Field> get fields => [title, content, date];
}

class DbMinContent extends DbContent {
  final text = stringField('text');

  @override
  List<Field> get fields => [text];
}

void main() {
  var provider = inMemoryDatabaseProvider;
  providerTest(provider);
}

void providerTest(DatabaseProvider provider) {
  group('Db', () {
    test('delete', () async {
      var provider = inMemoryDatabaseProvider;
      var db = 'test.db';
      await provider.deleteDatabase(db);
    });
    Future<String> preparedDbName() async {
      var name = 'test.db';
      await provider.deleteDatabase(name);
      return name;
    }

    test('open/close', () async {
      var provider = inMemoryDatabaseProvider;
      var name = 'test.db';
      await provider.deleteDatabase(name);
      var db = await provider.openDatabase(name, options: DatabaseOptions());
      await db.close();
    });
    test('add/get', () async {
      var store = StoreRef('note');
      var note = DbNote()
        ..title.v = 'my_title'
        ..content.v = 'my_content'
        ..date.v = 1234;
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbNote());
              }));
      var key = await store.addOld(db, note);
      expect(key, 1);
      var snapshot = await store.record(key).getDbSnapshot(db);
      expect(snapshot.fields.contains(Field('title')..v = 'my_title'), isTrue);
      expect(snapshot.field<String>('title').v, 'my_title');
      var readNote = DbNote();
      snapshot = await store.record(key).getDbSnapshot(db, content: readNote);
      expect(snapshot.field<String>('content').v, 'my_content');
      expect(readNote.content.v, 'my_content');

      var snapshots = await store.query().getOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1}
      ]);

      var note2 = DbNote()
        ..title.v = 'other_title'
        ..content.v = 'other_content'
        ..date.v = 5678;
      var key2 = await store.addOld(db, note2);

      snapshots = await store.query().getOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      snapshots = await store.query().allOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      expect((await store.record(key).getDbSnapshot(db)).toDebugMap(), {
        'title': 'my_title',
        'content': 'my_content',
        'date': 1234,
        '_id': 1
      });
      expect((await store.record(key2).getDbSnapshot(db)).toDebugMap(), {
        'title': 'other_title',
        'content': 'other_content',
        'date': 5678,
        '_id': 2
      });

      snapshots = await store.query(finder: Finder(limit: 1)).getOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
      ]);
      snapshots = await store.query(finder: Finder(offset: 1)).getOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);
      var resultSet = await store.query(finder: Finder(limit: 1)).getOld(db);
      snapshots = await store.query().getOld(db, cursor: resultSet.cursor);
      expect(snapshotsToDebugMap(snapshots), [
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);

      // Wrong access, kept for testing.
      snapshots = await store.query(finder: Finder(limit: 1)).allOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'title': 'my_title', 'content': 'my_content', 'date': 1234, '_id': 1},
        {
          'title': 'other_title',
          'content': 'other_content',
          'date': 5678,
          '_id': 2
        }
      ]);

      await db.close();
    });

    test('get object', () async {
      var store = StoreRef('test');
      var content = DbMinContent()..text.v = 'text';
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbMinContent());
              }));
      var key = await store.addOld(db, content);
      content =
          await store.record(key).getDbSnapshot(db, content: DbMinContent());
      expect(content.toDebugMap(), {'text': 'text', '_id': 1});

      await db.close();
    });

    test('clear', () async {
      var store = StoreRef('test');
      var content = DbMinContent()..text.v = 'text';
      var name = await preparedDbName();
      var db = await provider.openDatabase(name,
          options: DatabaseOptions(
              version: 1,
              onVersionChanged: (txn, oldVersion, newVersion) {
                txn.addStoreOld(store, DbMinContent());
              }));
      var key = await store.addOld(db, content);
      var snapshots = await store.query().getOld(db);
      expect(snapshotsToDebugMap(snapshots), [
        {'text': 'text', '_id': 1}
      ]);
      await store.clear(db);
      expect(await store.record(key).getDbSnapshot(db), isNull);

      await db.close();
    });
  });
}

List<Map<String, dynamic>> snapshotsToDebugMap(Iterable<DbSnapshot> snapshots) {
  return snapshots.map((snapshot) => snapshotToDebugMap(snapshot)).toList();
}

Map<String, dynamic> snapshotToDebugMap(DbSnapshot snapshot) {
  return snapshot.toDebugMap();
}
