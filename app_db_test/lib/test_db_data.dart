import 'package:tekartik_app_db/app_db.dart';

class DbNote extends DbContent implements ContentWithIndexedFields {
  final title = stringField('title');
  final content = stringField('content');
  final date = intField('date');

  @override
  List<Field> get fields => [title, content, date];

  @override
  List<Field> get indexedFields => [date];
}
