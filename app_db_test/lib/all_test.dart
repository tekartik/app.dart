import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db_test/db_schema_test.dart';

import 'app_db_index_test.dart' as db_index_test;
import 'app_db_test.dart' as db_test;

void main() {
  var provider = inMemoryDatabaseProvider;
  allTests(provider);
}

void allTests(DatabaseProvider provider) {
  db_test.providerTest(provider);
  db_index_test.providerTest(provider);
  providerDbSchemaGroup(provider);
}
