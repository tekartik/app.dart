@TestOn('vm')
import 'package:sembast/sembast_io.dart';
import 'package:tekartik_app_db/app_db_sembast.dart';
import 'package:tekartik_app_db_test/all_test.dart';
import 'package:test/test.dart';

void main() {
  var provider = getDatabaseProviderSembast(
      sembastDatabaseFactory: createDatabaseFactoryIo(
          rootPath: '.dart_tool/tekartik_app_db/test/sembast_io'));
  allTests(provider);
}
