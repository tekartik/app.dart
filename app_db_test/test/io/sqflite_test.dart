@TestOn('vm')
import 'package:sqflite_common/sqflite_dev.dart';
import 'package:sqflite_common/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:tekartik_app_db/src/sqflite/sqflite_provider.dart';
import 'package:tekartik_app_db_test/all_test.dart';
import 'package:tekartik_app_db_test/src/mixin.dart';
import 'package:test/test.dart';

void main() {
  sqfliteFfiInit();
  // ignore: deprecated_member_use
  appDbSqfliteDebug = true;
  // ignore: deprecated_member_use
  databaseFactoryFfi.setLogLevel(sqfliteLogLevelVerbose);
  var provider = getDatabaseProviderSqflite(databaseFactoryFfi);
  allTests(provider);
}
