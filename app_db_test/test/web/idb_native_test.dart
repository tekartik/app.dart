@TestOn('browser')
import 'package:idb_shim/idb_client_native.dart';
import 'package:tekartik_app_db/src/idb/idb_provider.dart';
import 'package:tekartik_app_db_test/all_test.dart';
import 'package:test/test.dart';

void main() {
  var provider = getDatabaseProviderIdb(idbFactoryNative);
  allTests(provider);
}
