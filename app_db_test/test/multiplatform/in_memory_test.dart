import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db_test/all_test.dart';

void main() {
  var provider = inMemoryDatabaseProvider;
  allTests(provider);
}
