import 'package:idb_shim/idb_client_memory.dart';
import 'package:tekartik_app_db/src/idb/idb_provider.dart';
import 'package:tekartik_app_db_test/all_test.dart';
import 'package:tekartik_app_db_test/src/import.dart';
import 'package:tekartik_app_db_test/src/mixin.dart';

void main() {
  // ignore: deprecated_member_use
  appDbIdbDebug = devWarning(true); // false
  var provider = getDatabaseProviderIdb(idbFactoryMemory);
  allTests(provider);
}
