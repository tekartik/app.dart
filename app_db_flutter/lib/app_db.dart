// @dart=2.9
import 'package:tekartik_app_db/app_db.dart';

import 'src/app_db.dart' as src;

/// To call only once
DatabaseProvider getDatabaseProvider({String packageName, String rootPath}) =>
    src.getDatabaseProvider(packageName: packageName, rootPath: rootPath);
