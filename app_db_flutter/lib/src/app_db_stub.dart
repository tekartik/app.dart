// @dart=2.9
import 'package:tekartik_app_db/app_db.dart';

DatabaseProvider getDatabaseProvider({String packageName, String rootPath}) =>
    _stub('getDatabaseProvider');

T _stub<T>(String message) {
  throw UnimplementedError(message);
}
