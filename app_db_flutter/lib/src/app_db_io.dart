// @dart=2.9
import 'package:tekartik_app_db/app_db.dart';
import 'package:tekartik_app_db/app_db_sqflite.dart';
import 'package:tekartik_app_flutter_sqflite/sqflite.dart';

DatabaseProvider getDatabaseProvider({String packageName, String rootPath}) =>
    getDatabaseProviderSqflite(
        getDatabaseFactory(packageName: packageName, rootPath: rootPath));
