## Setup

```yaml
tekartik_app_db_flutter:
  git:
    url: https://gitlab.com/tekartik/app.dart
    path: app_db_flutter
  version: '>=0.1.0'
```